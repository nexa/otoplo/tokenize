import { ElectronAPI } from '@electron-toolkit/preload';

declare interface ApiInterface {
  exportFile: (file: Buffer, title: string) => Promise<boolean>;
  getVersion: () => Promise<string>;
}

declare global {
  interface Window {
    electron: ElectronAPI
    api: ApiInterface
  }
}
