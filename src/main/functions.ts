import { BrowserWindow, IpcMainInvokeEvent, dialog } from "electron";
import fs from 'fs/promises';

export async function exportFile(event: IpcMainInvokeEvent, file: Buffer, title: string, mainWindow: BrowserWindow) {
  let path = dialog.showSaveDialogSync(mainWindow, { defaultPath: `${title}.zip` });
  if (!path) {
    return false;
  }

  try {
    await fs.writeFile(path, file);
    return true;
  } catch (e) {
    throw new Error('Unable to save file')
  }
}