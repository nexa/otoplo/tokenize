import Dexie, { Table } from "dexie";
import { NftEntity, TokenEntity, TransactionEntity } from "./entities";

export class AppDB extends Dexie {

  transactions!: Table<TransactionEntity>;
  tokens!: Table<TokenEntity>;
  nfts!: Table<NftEntity>;

  constructor() {
    super('tokenizeDb');
    this.version(1).stores({
        transactions: 'idem, time, group, extraGroup',
        tokens: 'tokenIdHex, &token, parentGroup, addedTime',
        nfts: 'tokenIdHex, &token, parentGroup, addedTime'
    });
  }

  async clearData() {
    await this.transactions.clear();
    await this.tokens.clear();
    await this.nfts.clear();
  }
}

export const appdb = new AppDB();