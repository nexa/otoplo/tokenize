import { appdb } from "./app-db";
import { TokenEntity } from "./entities";

class TokensRepository {

  public async upsert(token: TokenEntity) {
    return await appdb.tokens.put(token, token.tokenIdHex);
  }

  public async findAll() {
    return await appdb.tokens.orderBy('addedTime').reverse().toArray();
  }

  public async count() {
    return await appdb.tokens.count();
  }

  public async findById(token?: string) {
    if (token) {
      return await appdb.tokens.where('tokenIdHex').equals(token).or('token').equals(token).first();
    }
    return undefined;
  }

  public async findAllParents() {
    return await appdb.tokens.where('parentGroup').equals("").reverse().sortBy('addedTime');
  }

  public async findAllSubgroups() {
    return await appdb.tokens.where('parentGroup').notEqual("").reverse().sortBy('addedTime');
  }

  public async findSubgroupsByParent(parent: string) {
    return await appdb.tokens.where('parentGroup').equals(parent).reverse().sortBy('addedTime');
  }
}

export const tokensRepository = new TokensRepository();