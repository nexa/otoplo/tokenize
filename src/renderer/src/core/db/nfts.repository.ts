import { appdb } from "./app-db";
import { NftEntity } from "./entities";

class NftsRepository {

  public async upsert(nft: NftEntity) {
    return await appdb.nfts.put(nft, nft.tokenIdHex);
  }

  public async findAll() {
    return await appdb.nfts.orderBy('addedTime').reverse().toArray();
  }

  public async count() {
    return await appdb.nfts.count();
  }

  public async findById(nft?: string) {
    if (nft) {
      return await appdb.nfts.where('tokenIdHex').equals(nft).or('token').equals(nft).first();
    }
    return undefined;
  }

  public async findAllByParent(parent?: string) {
    if (parent) {
      return await appdb.nfts.where('parentGroup').equals(parent).sortBy('addedTime');
    }
    return [];
  }

  public async delete(id: string) {
    return await appdb.nfts.delete(id);
  }
}

export const nftsRepository = new NftsRepository();