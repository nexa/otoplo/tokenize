export type TxEntityState = 'incoming' | 'outgoing' | 'both';

export interface TransactionEntity {
  id: string;
  idem: string;
  time: number;
  height: number;
  payTo: string;
  state: TxEntityState;
  value: string;
  fee: number;
  group: string;
  extraGroup: string;
  txGroupType: number;
  tokenAmount: string;
}

export interface TokenEntity {
  token: string;
  tokenIdHex: string;
  name: string;
  ticker: string;
  summary: string;
  iconUrl: string;
  docUrl: string;
  docHash: string;
  decimals: number;
  parentGroup: string;
  addedTime: number;
}

export interface NftEntity {
  token: string;
  tokenIdHex: string;
  parentGroup: string;
  zipData: Buffer;
  addedTime: number;
}