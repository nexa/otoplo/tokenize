import nexcore from 'nexcore-lib';
import { WalletKeys } from './interfaces';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import { rostrumProvider } from '@renderer/providers/rostrumProvider';
import PrivateKey from 'nexcore-lib/types/lib/privatekey';
import { TxTokenType, isValidNexaAddress } from './walletUtils';
import { TransactionEntity, TxEntityState } from '../db/entities';
import { transactionsRepository } from '../db/transactions.repository';
import { currentTimestamp, isNullOrEmpty } from '@renderer/utils/utils';
import { PermissionLable } from '@renderer/components/atoms/permission-toggle';

export async function buildAndSignTransferTransaction(keys: WalletKeys, toAddr: string, amount: string, token?: string) {
    let tx = prepareTransaction(toAddr, amount, token);
    let tokenPrivKeys: PrivateKey[] = [];
    if (token) {
        tokenPrivKeys = await populateTokenInputsAndChange(tx, keys, token, BigInt(amount));
    }
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    return await finalizeTransaciton(tx, privKeys.concat(tokenPrivKeys));
}

export async function buildAndSignMintTransaction(keys: WalletKeys, amount: string, token: string) {
    let toAddr = keys.receiveKeys.at(-1)!.address;
    let tx = prepareTransaction(toAddr, amount, token);
    let authKey = await populateTokenAuth(tx, keys, token, 'mint');
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    privKeys.push(authKey);
    return await finalizeTransaciton(tx, privKeys);
}

export async function buildAndSignMeltTransaction(keys: WalletKeys, amount: string, token: string) {
    let tx = new nexcore.Transaction();
    let authKey = await populateTokenAuth(tx, keys, token, 'melt');
    let tokenPrivKeys = await populateTokenInputsAndChange(tx, keys, token, BigInt(amount));
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    privKeys.push(authKey);
    return await finalizeTransaciton(tx, privKeys.concat(tokenPrivKeys));
}

export async function buildAndSignRenewTransaction(keys: WalletKeys, toAddr: string, amount: string, token: string, perms: PermissionLable[]) {
    let tx = prepareTransaction(toAddr, amount, token, true);
    let authKeys = await populateAndDuplicateTokenAuths(tx, keys, token, perms);
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    return await finalizeTransaciton(tx, privKeys.concat(authKeys));
}

export async function buildAndSignKillTransaction(keys: WalletKeys, outpoint: string) {
    let [tx, authKey] = await prepareKillTransaction(keys, outpoint);
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    privKeys.push(authKey);
    return await finalizeTransaciton(tx, privKeys);
}

export async function buildAndSignCreateGroupTransaction(keys: WalletKeys, name: string, ticker: string, decimals: number, docUrl?: string, docHash?: string) {
    let [tx, privKeys, idHex, signKey] = await buildCreateGroupTransaction(keys, name, ticker, decimals, docUrl, docHash);
    let tokenId = new nexcore.Address(Buffer.from(idHex, 'hex'), nexcore.Networks.livenet, nexcore.Address.GroupIdAddress);
    return { tx: await finalizeTransaciton(tx, privKeys), idHex: idHex, tokenId: tokenId.toString(), signKey: signKey! };
}

export async function buildAndSignCreateSubgroupTransaction(keys: WalletKeys, parent: string, data: string, name: string, ticker: string, decimals: number, docUrl?: string, docHash?: string) {
    let [tx, subgroup] = prepareSubgroupTransaction(parent, data, name, ticker, decimals, docUrl, docHash);
    let subgroupKey = keys.receiveKeys.at(-1);
    let authKey = await populateTokenAuth(tx, keys, parent, 'subgroup', subgroup, subgroupKey!.address);
    let privKeys = await populateNexaInputsAndChange(tx, keys);
    privKeys.push(authKey);
    let tokenId = new nexcore.Address(Buffer.from(subgroup, 'hex'), nexcore.Networks.livenet, nexcore.Address.GroupIdAddress);
    return { tx: await finalizeTransaciton(tx, privKeys), idHex: subgroup, tokenId: tokenId.toString(), signKey: subgroupKey!.key.getPrivateKey() };
}

function prepareSubgroupTransaction(parent: string, data: string, name: string, ticker: string, decimals: number, docUrl?: string, docHash?: string): [Transaction, string] {
    let tx = new nexcore.Transaction();
    let opReturn = nexcore.GroupToken.buildTokenDescScript(ticker, name, docUrl ?? '', docHash ?? '', decimals);
    tx.addGroupData(opReturn);

    return [tx, nexcore.GroupToken.generateSubgroupId(parent, data).toString('hex')];
}

async function buildCreateGroupTransaction(
    keys: WalletKeys,
    name: string,
    ticker: string,
    decimals: number,
    docUrl?: string,
    docHash?: string
): Promise<[Transaction, PrivateKey[], string, PrivateKey | undefined]> {
    let tx = new nexcore.Transaction();
    let allKeys = keys.receiveKeys.concat(keys.changeKeys);

    let opReturn = nexcore.GroupToken.buildTokenDescScript(ticker, name, docUrl ?? '', docHash ?? '', decimals);
    tx.addGroupData(opReturn);

    let outpoint = '', idHex = '';
    let usedKeys: PrivateKey[] = [];
    let signKey: PrivateKey | undefined = undefined;
    for (let key of allKeys) {
        let utxos = await rostrumProvider.getNexaUtxos(key.address);
        for (let utxo of utxos) {
            tx.from({
                txId: utxo.outpoint_hash,
                outputIndex: utxo.tx_pos,
                address: key.address,
                satoshis: utxo.value
            });

            if (isNullOrEmpty(outpoint)) {
                outpoint = utxo.outpoint_hash;
                let id = nexcore.GroupToken.findGroupId(Buffer.from(outpoint, 'hex'), opReturn.toBuffer(), nexcore.GroupToken.authFlags.ACTIVE_FLAG_BITS);
                tx.toGrouped(keys.receiveKeys.at(-1)!.address, id.hashBuffer, nexcore.GroupToken.authFlags.ACTIVE_FLAG_BITS | id.nonce);
                idHex = id.hashBuffer.toString('hex');
                signKey = keys.receiveKeys.at(-1)!.key.getPrivateKey();
            }

            usedKeys.push(key.key.getPrivateKey());
            if (tx._getUnspentValue() > 0) {
                tx.change(keys.changeKeys.at(-1)!.address);
                if (tx._estimateSize() * (nexcore.Transaction.FEE_PER_KB / 1000) <= tx.getFee()) {
                    return [tx, usedKeys, idHex, signKey];
                }
            }
        }
    }

    throw new Error("Not enough Nexa balance.");
}

async function prepareKillTransaction(keys: WalletKeys, outpoint: string): Promise<[Transaction, PrivateKey]> {
    let utxo = await rostrumProvider.getUtxo(outpoint);
    let address = utxo.addresses[0];

    let tx = new nexcore.Transaction();
    tx.from({
        txId: outpoint,
        outputIndex: utxo.tx_pos,
        address: address,
        satoshis: utxo.amount
    });

    let allKeys = keys.receiveKeys.concat(keys.changeKeys);
    let addrKey = allKeys.find(k => k.address === address);

    if (!addrKey) {
        throw new Error('UTXO associated key not found in the wallet'); 
    }
    return [tx, addrKey.key.getPrivateKey()];
}

function prepareTransaction(toAddr: string, amount: string, token?: string, isAuth = false) {
    if (!isValidNexaAddress(toAddr) && !isValidNexaAddress(toAddr, nexcore.Address.PayToPublicKeyHash)) {
        throw new Error('Invalid Address.');
    }
    if ((token && !isAuth && BigInt(amount) < 1n) || (!token && parseInt(amount) < nexcore.Transaction.DUST_AMOUNT)) {
        throw new Error("The amount is too small.");
    }

    let outType = nexcore.Address.getOutputType(toAddr);
    let tx = new nexcore.Transaction();

    if (token) {
        if (!isValidNexaAddress(token, nexcore.Address.GroupIdAddress)) {
            throw new Error('Invalid Token ID');
        }
        if (outType === 0) {
            throw new Error('Token must be sent to script template address');
        }
        tx.toGrouped(toAddr, token, BigInt(amount));
    } else {
        tx.to(toAddr, parseInt(amount), outType);
    }

    return tx;
}

async function populateTokenInputsAndChange(tx: Transaction, keys: WalletKeys, token: string, outTokenAmount: bigint) {
    let allKeys = keys.receiveKeys.concat(keys.changeKeys);
    let usedKeys: PrivateKey[] = [];
    let inTokenAmount = 0n;

    for (let key of allKeys) {
        let utxos = await rostrumProvider.getTokenUtxos(key.address, token);
        for (let utxo of utxos) {
            if (utxo.token_amount < 0) {
                continue;
            }
            tx.from({
                txId: utxo.outpoint_hash,
                outputIndex: utxo.tx_pos,
                address: key.address,
                satoshis: utxo.value
            });

            inTokenAmount = inTokenAmount + BigInt(utxo.token_amount);
            usedKeys.push(key.key.getPrivateKey());

            if (inTokenAmount == outTokenAmount) {
                return usedKeys;
            }
            if (inTokenAmount > outTokenAmount) {
                tx.toGrouped(keys.changeKeys.at(-1)!.address, token, inTokenAmount - outTokenAmount);
                return usedKeys;
            }
        }
    }

    throw new Error("Not enough token balance");
}

async function populateTokenAuth(tx: Transaction, keys: WalletKeys, token: string, perm: PermissionLable, subgroup = '', subgroupAddr ='') {
    let allKeys = keys.receiveKeys.concat(keys.changeKeys);
    for (let key of allKeys) {
        let utxos = await rostrumProvider.getTokenUtxos(key.address, token);
        for (let utxo of utxos) {
            if (!isAuthFit(utxo.token_amount, perm)) {
                continue;
            }

            tx.from({
                txId: utxo.outpoint_hash,
                outputIndex: utxo.tx_pos,
                address: key.address,
                satoshis: utxo.value
            });

            if (perm === 'subgroup') {
                tx.toGrouped(subgroupAddr, subgroup, dupAuthority(utxo.token_amount, false));
            }

            // if renew flag included, we don't want to burn it
            if (nexcore.GroupToken.allowsRenew(BigInt.asUintN(64, BigInt(utxo.token_amount)))) {
                tx.toGrouped(keys.receiveKeys.at(-1)!.address, token, dupAuthority(utxo.token_amount));
            }

            return key.key.getPrivateKey();
        }
    }

    throw new Error("The requested authority not found");
}

async function populateAndDuplicateTokenAuths(tx: Transaction, keys: WalletKeys, token: string, perms: PermissionLable[]) {
    let allKeys = keys.receiveKeys.concat(keys.changeKeys);
    let usedKeys: PrivateKey[] = [];

    let reqiredPerms = new Set(perms);
    reqiredPerms.add('authorise');

    for (let key of allKeys) {
        let utxos = await rostrumProvider.getTokenUtxos(key.address, token);
        for (let utxo of utxos) {
            if (utxo.token_amount > 0) {
                continue;
            }

            let found = false;
            for (let perm of reqiredPerms) {
                if (isAuthFit(utxo.token_amount, perm)) {
                    reqiredPerms.delete(perm);
                    found = true;
                }
            }

            if (!found) {
                continue;
            }

            tx.from({
                txId: utxo.outpoint_hash,
                outputIndex: utxo.tx_pos,
                address: key.address,
                satoshis: utxo.value
            });
            usedKeys.push(key.key.getPrivateKey());

            // duplicate
            tx.toGrouped(keys.receiveKeys.at(-1)!.address, token, dupAuthority(utxo.token_amount));

            if (reqiredPerms.size === 0) {
                return usedKeys;
            }
        }
    }

    throw new Error("The required authorities not found");
}

async function populateNexaInputsAndChange(tx: Transaction, keys: WalletKeys) {
    let allKeys = keys.receiveKeys.concat(keys.changeKeys);
    let usedKeys: PrivateKey[] = [];

    for (let key of allKeys) {
        let utxos = await rostrumProvider.getNexaUtxos(key.address);
        for (let utxo of utxos) {
            tx.from({
                txId: utxo.outpoint_hash,
                outputIndex: utxo.tx_pos,
                address: key.address,
                satoshis: utxo.value
            });

            usedKeys.push(key.key.getPrivateKey());
            if (tx._getUnspentValue() > 0) {
                tx.change(keys.changeKeys.at(-1)!.address);
                if (tx._estimateSize() * (nexcore.Transaction.FEE_PER_KB / 1000) <= tx.getFee()) {
                    return usedKeys;
                }
            }
        }
    }

    throw new Error("Not enough Nexa balance.");
}

async function finalizeTransaciton(tx: Transaction, privKeys: PrivateKey[]) {
    let tip = await rostrumProvider.getBlockTip();
    return tx.lockUntilBlockHeight(tip.height).sign(privKeys);
}

export async function broadcastTransaction(txHex: string) {
    return await rostrumProvider.broadcast(txHex);
}

export async function saveLocalTransaction(
    tx: Transaction,
    idem: string,
    destination: string,
    amount: string,
    txState: TxEntityState,
    txType: TxTokenType,
    token?: string,
    parent?: string
) {
    try {
        let txEntry: TransactionEntity = {
            extraGroup: parent ? parent : "none",
            fee: tx.getFee(),
            group: token ? token : "none",
            height: 0,
            id: tx.id,
            idem: idem,
            payTo: destination,
            state: txState,
            time: currentTimestamp(),
            tokenAmount: token ? amount : "0",
            txGroupType: txType,
            value: token ? tx.getFee().toString() : (parseInt(amount) + tx.getFee()).toString()
        }
    
        await transactionsRepository.upsert(txEntry);
    } catch (e) {
        console.error(e) // can be ignored since it will be updated on sync
    }
}

function isAuthFit(authFlags: bigint | number, permission: PermissionLable) {
    if (authFlags > 0) {
        return false;
    }

    let flags = BigInt.asUintN(64, BigInt(authFlags));
    switch (permission) {
        case 'authorise':
            return nexcore.GroupToken.allowsRenew(flags);
        case 'mint':
            return nexcore.GroupToken.allowsMint(flags);
        case 'melt':
            return nexcore.GroupToken.allowsMelt(flags);
        case 'rescript':
            return nexcore.GroupToken.allowsRescript(flags);
        case 'subgroup':
            return nexcore.GroupToken.allowsSubgroup(flags);            
        default:
            return false;
    }
}

function dupAuthority(authFlags: bigint | number, withSubgroup = true) {
    if (authFlags > 0) {
        return 0n;
    }

    let flags = BigInt.asUintN(64, BigInt(authFlags));
    let newFlags = nexcore.GroupToken.authFlags.AUTHORITY;

    if (nexcore.GroupToken.allowsRenew(flags)) {
        newFlags |= nexcore.GroupToken.authFlags.BATON;
    }
    if (nexcore.GroupToken.allowsMint(flags)) {
        newFlags |= nexcore.GroupToken.authFlags.MINT;
    }
    if (nexcore.GroupToken.allowsMelt(flags)) {
        newFlags |= nexcore.GroupToken.authFlags.MELT;
    }
    if (nexcore.GroupToken.allowsRescript(flags)) {
        newFlags |= nexcore.GroupToken.authFlags.RESCRIPT;
    }
    if (nexcore.GroupToken.allowsSubgroup(flags) && withSubgroup) {
        newFlags |= nexcore.GroupToken.authFlags.SUBGROUP;
    }

    return newFlags;
}

export function buildAuthority(perms: PermissionLable[]) {
    let newFlags = nexcore.GroupToken.authFlags.AUTHORITY;
    for (let perm of perms) {
        switch (perm) {
            case 'authorise':
                newFlags |= nexcore.GroupToken.authFlags.BATON;
                break;
            case 'mint':
                newFlags |= nexcore.GroupToken.authFlags.MINT;
                break;
            case 'melt':
                newFlags |= nexcore.GroupToken.authFlags.MELT;
                break;
            case 'rescript':
                newFlags |= nexcore.GroupToken.authFlags.RESCRIPT;
                break;
            case 'subgroup':
                newFlags |= nexcore.GroupToken.authFlags.SUBGROUP;
                break;
        }
    }
    return newFlags;
}