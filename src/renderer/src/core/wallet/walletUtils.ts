import * as Bip39 from 'bip39';
import { rostrumProvider } from '../../providers/rostrumProvider';
import StorageProvider from '../../providers/storageProvider';
import NexCore from 'nexcore-lib';
import { AddressKey, Balance, WalletIndexes, WalletKeys } from './interfaces';
import HDPrivateKey from 'nexcore-lib/types/lib/hdprivatekey';
import { ITXHistory, ITXInput, ITXOutput } from '../models/transaction';
import bigDecimal from 'js-big-decimal';
import { currentTimestamp, isNullOrEmpty } from '../../utils/utils';
import { NftEntity, TokenEntity, TransactionEntity, TxEntityState } from '../db/entities';
import { appdb } from '../db/app-db';
import { transactionsRepository } from '../db/transactions.repository';
import NiftyProvider from '@renderer/providers/niftyProvider';
import { nftsRepository } from '../db/nfts.repository';
import { tokensRepository } from '../db/tokens.repository';
import { toast } from 'react-toastify';

export enum TxTokenType {
    NO_GROUP,
    CREATE,
    MINT,
    MELT,
    RENEW,
    TRANSFER
}

export function parseTokenTxAction(txGroupType: TxTokenType, txState: TxEntityState) {
    switch (txGroupType) {
        case TxTokenType.CREATE:
            return 'create';
        case TxTokenType.MINT:
            return 'mint';
        case TxTokenType.MELT:
            return 'melt';
        case TxTokenType.RENEW:
            return 'authorise';
        case TxTokenType.TRANSFER:
            if (txState === 'incoming') {
                return 'receive';
            } else if (txState === 'outgoing') {
                return 'send';
            } else {
                return 'consolidate';
            }
        default:
            return '---'
    }
}

export async function clearLocalWallet() {
    StorageProvider.clearData();
    await appdb.clearData();
}

export function isValidNexaAddress(address: string, type = NexCore.Address.PayToScriptTemplate) {
    return NexCore.Address.isValid(address, NexCore.Networks.livenet, type);
}

export function generateAccountKey(mnemonic: string, passphrase?: string | undefined) {
    const seed = Bip39.mnemonicToSeedSync(mnemonic, passphrase);
    const accountKey = NexCore.HDPrivateKey.fromSeed(seed);
    return accountKey.deriveChild(44, true).deriveChild(29223, true).deriveChild(0, true);
}

export function generateKeysAndAddresses(accountKey: HDPrivateKey, fromRIndex: number, rIndex: number, fromCIndex: number, cIndex: number): WalletKeys {
    let receive = accountKey.deriveChild(0, false);
    let change = accountKey.deriveChild(1, false);
    let rKeys: AddressKey[] = [], cKeys: AddressKey[] = [];
    for (let index = fromRIndex; index < rIndex; index++) {
        let k = receive.deriveChild(index, false);
        let addr = k.getPublicKey().toAddress().toString();
        rKeys.push({key: k, address: addr});
    }
    for (let index = fromCIndex; index < cIndex; index++) {
        let k = change.deriveChild(index, false);
        let addr = k.getPublicKey().toAddress().toString();
        cKeys.push({key: k, address: addr});
    }
    return {receiveKeys: rKeys, changeKeys: cKeys};
}

export async function discoverWallet(accountKey: HDPrivateKey) : Promise<WalletIndexes> {
    let receiveKey = accountKey.deriveChild(0, false);
    let changeKey = accountKey.deriveChild(1, false);

    let rIndexPromise = discoverWalletIndex(receiveKey);
    let cIndexPromise = discoverWalletIndex(changeKey);

    let [rIndex, cIndex] = await Promise.all([rIndexPromise, cIndexPromise]);

    let indexes: WalletIndexes = { receiveIndex: rIndex+1, changeIndex: cIndex+1 };
    StorageProvider.saveWalletIndexes(indexes);

    return indexes;
}

async function discoverWalletIndex(deriveKey: HDPrivateKey) {
    let index = 0, stop = false, addrBatch = 0;

    do {
        stop = true;
        for (let i = addrBatch; i < addrBatch+20; i++) {
            let rAddr = deriveKey.deriveChild(i, false).getPublicKey().toAddress().toString();
            let isUsed = await isAddressUsed(rAddr);
            if (isUsed) {
                index++;
                stop = false;
            }
        }
        addrBatch += 20;
    } while (!stop);

    return index;
}

async function isAddressUsed(address: string) {
    try {
        let firstUse = await rostrumProvider.getFirstUse(address);
        return firstUse.tx_hash && firstUse.tx_hash !== "";
    } catch (e) {
        if (e instanceof Error && e.message.includes("not found")) {
            return false;
        }
        throw e;
    }
}

export async function fetchTotalBalance(addresses: string[]) {
    let promises: Promise<Balance>[] = [];
    addresses.forEach(address => {
        let b = rostrumProvider.getBalance(address);
        promises.push(b);
    });

    return await Promise.all(promises);
}

export function sumBlance(balances: Balance[]): Balance {
    let confirmed = new bigDecimal(0), unconfirmed = new bigDecimal(0);
    balances.forEach(b => {
        confirmed = confirmed.add(new bigDecimal(b.confirmed));
        unconfirmed = unconfirmed.add(new bigDecimal(b.unconfirmed));
    });
    return {confirmed: confirmed.getValue(), unconfirmed: unconfirmed.getValue()};
}

export async function fetchTransactionHistory(addresses: string[], fromHeight: number) {
    let index = 0, i = 0, data = new Map<string, ITXHistory>(), maxHeight = fromHeight;

    for (let address of addresses) {
        i++;
        let txHistory = await rostrumProvider.getTransactionsHistory(address);
        if (txHistory && txHistory.length > 0) {
            index = i;
            for (let tx of txHistory) {
                if (tx.height === 0 || tx.height > fromHeight) {
                    maxHeight = Math.max(maxHeight, tx.height);
                    data.set(tx.tx_hash, tx);
                }
            }
        }
    }

    return {index: index, txs: data, lastHeight: maxHeight};
}

export async function classifyAndSaveTransaction(txHistory: ITXHistory, myAddresses: string[], correlationId: string) {
    let t = await rostrumProvider.getTransaction(txHistory.tx_hash);

    let outputs = t.vout.filter(utxo => !isNullOrEmpty(utxo.scriptPubKey.addresses));

    let isOutgoing = t.vin.length > 0 && myAddresses.includes(t.vin[0].addresses[0]);
    let isIncoming = !isOutgoing || outputs.every(utxo => myAddresses.includes(utxo.scriptPubKey.addresses[0]));
    let isConfirmed = t.height > 0;

    let txEntry = {} as TransactionEntity;
    txEntry.id = t.txid;
    txEntry.idem = t.txidem;
    txEntry.height = isConfirmed ? t.height : 0;
    txEntry.time = isConfirmed ? t.time : currentTimestamp();
    txEntry.fee = t.fee_satoshi;

    if (isOutgoing && isIncoming) {
        txEntry.state = 'both';
        txEntry.value = "0";
        txEntry.payTo = "Payment to yourself";
    } else if (isIncoming) {
        txEntry.state = 'incoming';
        let utxos = outputs.filter(utxo => myAddresses.includes(utxo.scriptPubKey.addresses[0]));
        let amount = new bigDecimal(0);
        utxos.forEach(utxo => {
            amount = amount.add(new bigDecimal(utxo.value_satoshi));
        });
        txEntry.value = amount.getValue();
        txEntry.payTo = utxos[0].scriptPubKey.addresses[0];
    } else if(isOutgoing) {
        txEntry.state = 'outgoing';
        let utxos = outputs.filter(utxo => !myAddresses.includes(utxo.scriptPubKey.addresses[0]));
        let amount = new bigDecimal(0);
        utxos.forEach(utxo => {
            amount = amount.add(new bigDecimal(utxo.value_satoshi));
        });
        txEntry.value = amount.getValue();
        txEntry.payTo = utxos[0].scriptPubKey.addresses[0];
    }

    let [txType, txGroup, tokenAmount, extraGroup] = classifyTokenTransaction(t.vin, outputs, txEntry.state, myAddresses);
    txEntry.txGroupType = txType;
    txEntry.group = txGroup;
    txEntry.tokenAmount = tokenAmount;
    txEntry.extraGroup = extraGroup;

    if (txGroup !== 'none' && NiftyProvider.isNiftySubgroup(txGroup)) {
        if (txEntry.state === 'incoming') {
            await fetchAndSaveNFT(txGroup, NiftyProvider.NIFTY_TOKEN.token);
        } else if (txEntry.state === 'outgoing') {
            await removeLocalNFT(txGroup);
        }
        txEntry.extraGroup = NiftyProvider.NIFTY_TOKEN.token;
    }

    await notifyIfNeeded(txEntry.idem, correlationId);
    await transactionsRepository.upsert(txEntry);

    return txEntry;
}

function classifyTokenTransaction(vin: ITXInput[], vout: ITXOutput[], txState: TxEntityState, myAddresses: string[]): [TxTokenType, string, string, string] {
    let groupInputs = vin.filter(input => !isNullOrEmpty(input.group));
    let groupOutputs = vout.filter(output => !isNullOrEmpty(output.scriptPubKey.group));

    if (isNullOrEmpty(groupInputs) && isNullOrEmpty(groupOutputs)) {
        return [TxTokenType.NO_GROUP, "none", "0", "none"];
    }

    let myGroupInputs = groupInputs.filter(input => myAddresses.includes(input.addresses[0]));
    let myGroupOutputs = groupOutputs.filter(output => myAddresses.includes(output.scriptPubKey.addresses[0]));

    if (isNullOrEmpty(myGroupInputs) && isNullOrEmpty(myGroupOutputs)) {
        return [TxTokenType.NO_GROUP, "none", "0", "none"];
    }

    if (isNullOrEmpty(groupInputs)) {
        let group = myGroupOutputs.find(output => BigInt(output.scriptPubKey.groupQuantity) < 0n)?.scriptPubKey.group ?? "none";
        return [TxTokenType.CREATE, group, "0", "none"];
    }

    if (isNullOrEmpty(groupOutputs)) {
        if (txState === 'incoming') {
            return [TxTokenType.NO_GROUP, "none", "0", "none"];
        }

        let inputs = myGroupInputs.filter(input => BigInt(input.groupQuantity) > 0n);
        if (!isNullOrEmpty(inputs)) {
            let amount = new bigDecimal(0);
            inputs.forEach(utxo => {
                amount = amount.add(new bigDecimal(utxo.groupQuantity));
            });
            let group = inputs[0].group;
            let extraGroup = myGroupInputs.find(input => BigInt(input.groupQuantity) < 0n && inputs[0].group != input.group)?.group ?? "none";
            return [TxTokenType.MELT, group, amount.getValue(), extraGroup];
        }

        let group = myGroupInputs.find(input => BigInt(input.groupQuantity) < 0n)?.group ?? "none";
        let extraGroup = myGroupInputs.find(input => BigInt(input.groupQuantity) < 0n && group != input.group)?.group ?? "none";
        return [TxTokenType.MELT, group, "0", extraGroup];
    }

    let tokenInputs = groupInputs.filter(input => BigInt(input.groupQuantity) > 0n);
    let tokenOutputs = groupOutputs.filter(output => BigInt(output.scriptPubKey.groupQuantity) > 0n);

    if (isNullOrEmpty(tokenInputs) && isNullOrEmpty(tokenOutputs)) {
        let group = groupInputs.find(input => BigInt(input.groupQuantity) < 0n)?.group ?? "none";
        let extraGroup = groupOutputs.find(output => BigInt(output.scriptPubKey.groupQuantity) < 0n && group != output.scriptPubKey.group)?.scriptPubKey.group ?? "none";
        return [TxTokenType.RENEW, extraGroup !== 'none' ? extraGroup : group, "0", extraGroup !== 'none' ? group : extraGroup];
    }
    
    if (isNullOrEmpty(tokenInputs)) {
        let group = tokenOutputs[0].scriptPubKey.group;
        let amount = new bigDecimal(0);
        tokenOutputs.forEach(utxo => {
            amount = amount.add(new bigDecimal(utxo.scriptPubKey.groupQuantity));
        });
        let extraGroup = groupInputs.find(input => BigInt(input.groupQuantity) < 0n && group != input.group)?.group ?? "none";
        return [TxTokenType.MINT, group, amount.getValue(), extraGroup];
    }

    if (isNullOrEmpty(tokenOutputs)) {
        let group = tokenInputs[0].group;
        let amount = new bigDecimal(0);
        tokenInputs.forEach(utxo => {
            amount = amount.add(new bigDecimal(utxo.groupQuantity));
        });
        let extraGroup = groupInputs.find(input => BigInt(input.groupQuantity) < 0n && group != input.group)?.group ?? "none";
        return [TxTokenType.MELT, group, amount.getValue(), extraGroup];
    }

    let outQuantitySum = tokenOutputs.map(output => BigInt(output.scriptPubKey.groupQuantity)).reduce((a, b) => a + b, 0n);
    let inQuantitySum = tokenInputs.map(input => BigInt(input.groupQuantity)).reduce((a, b) => a + b, 0n);

    if (outQuantitySum > inQuantitySum) {
        let group = tokenOutputs[0].scriptPubKey.group;
        let extraGroup = groupInputs.find(input => BigInt(input.groupQuantity) < 0n && group != input.group)?.group ?? "none";
        return [TxTokenType.MINT, group, (outQuantitySum - inQuantitySum).toString(), extraGroup];
    }

    if (inQuantitySum > outQuantitySum) {
        let group = tokenInputs[0].group;
        let extraGroup = groupInputs.find(input => BigInt(input.groupQuantity) < 0n && group != input.group)?.group ?? "none";
        return [TxTokenType.MELT, group, (inQuantitySum - outQuantitySum).toString(), extraGroup];
    }

    let group = tokenOutputs[0].scriptPubKey.group;
    let amount = "";
    if (txState === 'incoming') {
        amount = tokenOutputs
            .filter(output => myAddresses.includes(output.scriptPubKey.addresses[0]))
            .map(output => BigInt(output.scriptPubKey.groupQuantity))
            .reduce((a, b) => a + b, 0n)
            .toString();
    } else if (txState === 'outgoing') {
        amount = tokenOutputs
            .filter(output => !myAddresses.includes(output.scriptPubKey.addresses[0]))
            .map(output => BigInt(output.scriptPubKey.groupQuantity))
            .reduce((a, b) => a + b, 0n)
            .toString();
    } else {
        amount = "0";
    }

    return [TxTokenType.TRANSFER, group, amount, "none"];
}

async function fetchAndSaveNFT(token: string, parent: string) {
    // support only nifty for now
    try {
        let hexId = NexCore.Address.decodeNexaAddress(token).getHashBuffer().toString('hex');
        let nft = await NiftyProvider.fetchNFT(hexId);
        let nftEntity: NftEntity = {
            addedTime: currentTimestamp(),
            parentGroup: parent,
            token: token,
            tokenIdHex: hexId,
            zipData: nft
        }
        await nftsRepository.upsert(nftEntity);

        let isExistInDB = await tokensRepository.findById(NiftyProvider.NIFTY_TOKEN.tokenIdHex);
        if (!isExistInDB) {
            let tokenEntity: TokenEntity = {
                addedTime: currentTimestamp(),
                decimals: 0,
                docHash: NiftyProvider.NIFTY_TOKEN.docHash,
                docUrl: NiftyProvider.NIFTY_TOKEN.docUrl,
                iconUrl: NiftyProvider.NIFTY_TOKEN.iconUrl,
                name: NiftyProvider.NIFTY_TOKEN.name,
                ticker: NiftyProvider.NIFTY_TOKEN.ticker,
                parentGroup: '',
                summary: NiftyProvider.NIFTY_TOKEN.summary,
                token: NiftyProvider.NIFTY_TOKEN.token,
                tokenIdHex: NiftyProvider.NIFTY_TOKEN.tokenIdHex
            }
            await tokensRepository.upsert(tokenEntity);
        }
    } catch (e) {
        console.log('failed to fetch NFT from nifty');
    }
}

async function removeLocalNFT(token: string) {
    try {
        let hexId = NexCore.Address.decodeNexaAddress(token).getHashBuffer().toString('hex');
        await nftsRepository.delete(hexId);
    } catch (e) {
        console.log('failed to remove local NFT');
    }
}

async function notifyIfNeeded(txIdem: string, correlationId: string) {
    try {
        let exist = await transactionsRepository.findByIdem(txIdem);
        if (!exist) {
            toast.info("New transaction received!", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "dark",
                toastId: correlationId
            });
        }
    } catch (e) {
        console.log('failed to notify with toast');
    }
}