import { rostrumProvider } from '@renderer/providers/rostrumProvider';
import TokenApiProvider from '../../providers/tokenApiProvider';
import { TokenEntity } from '../db/entities';
import { ITokenUtxo } from '../models/listUnspent';
import NexCore from 'nexcore-lib';

export async function getTokenData(tid: string) {
    let res = await TokenApiProvider.getTokenInfo(tid);
    let tokenEntry = {} as TokenEntity;
    tokenEntry.token = res.tokenInfo.token;
    tokenEntry.tokenIdHex = res.tokenInfo.tokenIdHex;
    tokenEntry.name = res.tokenInfo.name ?? "";
    tokenEntry.ticker = res.tokenInfo.ticker ?? "";
    tokenEntry.docUrl = res.tokenInfo.documentUrl ?? "";
    tokenEntry.docHash = res.tokenInfo.documentHash ?? "";
    tokenEntry.decimals = res.tokenInfo.decimals ?? 0;
    tokenEntry.iconUrl = res.iconUrl ?? "";
    tokenEntry.parentGroup = res.tokenInfo.parentGroup ?? "";
    
    let json = res.jsonDoc ? JSON.parse(res.jsonDoc) : "";
    if (json) {
        tokenEntry.summary = json[0]?.summary ?? "";
        if (tokenEntry.summary && tokenEntry.summary.trim() && tokenEntry.summary.trim().at(-1) !== '.') {
            tokenEntry.summary += ".";  
        }
        tokenEntry.summary += " " + (json[0]?.description ?? "");
    }

    let supply = await TokenApiProvider.getTokenSupply(tokenEntry.token);
    
    return {tokenEntry: tokenEntry, supply: supply.rawSupply};
}

export async function fetchTokenUtxos(addresses: string[], token: string) {
    let promises: Promise<ITokenUtxo[]>[] = [];
    addresses.forEach(address => {
        let u = rostrumProvider.getTokenUtxos(address, token);
        promises.push(u);
    });

    return await Promise.all(promises);
}

export function classifyAuthority(amount: bigint, authorities: Set<string>) {
    let auths: Set<string> = new Set();
    if (!NexCore.GroupToken.isAuthority(amount)) {
        return auths;
    }

    if (NexCore.GroupToken.allowsMelt(amount)) {
        auths.add('melt');
        authorities.add('melt');
    }
    if (NexCore.GroupToken.allowsMint(amount)) {
        auths.add('mint');
        authorities.add('mint');        
    }
    if (NexCore.GroupToken.allowsRenew(amount)) {
        auths.add('baton');
        authorities.add('baton');
    }
    if (NexCore.GroupToken.allowsRescript(amount)) {
        auths.add('rescript');
        authorities.add('rescript');
    }
    if (NexCore.GroupToken.allowsSubgroup(amount)) {
        auths.add('subgroup');
        authorities.add('subgroup');
    }

    return auths;
}