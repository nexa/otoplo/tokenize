export interface ITransaction {
    blockhash: string;
    blocktime: number;
    confirmations: number;
    fee: number;
    fee_satoshi: number;
    hash: string;
    height: number;
    hex: string;
    locktime: number;
    size: number;
    time: number;
    txid: string;
    txidem: string;
    version: number;
    vin: ITXInput[];
    vout: ITXOutput[];
}

export interface ITXInput {
    outpoint: string;
    scriptSig: IScriptSig;
    sequence: number;
    value: number;
    value_satoshi: bigint | number;
    addresses: string[];
    group: string;
    groupAuthority: bigint | number;
    groupQuantity: bigint | number;
}

export interface ITXOutput {
    n: number;
    outpoint_hash: string;
    scriptPubKey: IScriptPubKey;
    type: number;
    value: number;
    value_satoshi: bigint | number;
}

export interface IScriptSig {
    asm: string;
    hex: string;
}

export interface IScriptPubKey {
    addresses: string[];
    argHash: string;
    asm: string;
    group: string;
    groupAuthority: bigint | number;
    groupQuantity: bigint | number;
    hex: string;
    scriptHash: string;
    token_id_hex?: string;
    type: string;
}

export interface ITXHistory {
    fee?: number;
    height: number;
    tx_hash: string;
}