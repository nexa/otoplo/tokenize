import { TokenEntity } from "@renderer/core/db/entities";
import { IErrorResponse } from "@renderer/core/models/tokenApi";
import axios, { AxiosRequestConfig } from "axios";
import nexcore from 'nexcore-lib';

export default class NiftyProvider {

    public static readonly NIFTY_TOKEN: TokenEntity = {
        name: 'NiftyArt',
        ticker: 'NIFTY',
        docUrl: 'https://niftyart.cash/td/nifty.json',
        docHash: '0b4abe023ae013fd9cc30c9e8b3fbf40a06beceb5048419bd01cc8480a91fab0',
        iconUrl: 'https://niftyart.cash/td/niftyicon.svg',
        parentGroup: '',
        summary: 'These are NiftyArt nonfungible tokens.',
        token: 'nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqqcg6kdm6f',
        tokenIdHex: 'cacf3d958161a925c28a970d3c40deec1a3fe06796fe1b4a7b68f377cdb90000',
        decimals: 0,
        addedTime: 0
    }

    private static readonly endpoint = import.meta.env.PROD ? "https://niftyart.cash/_public/" : "/_public/";

    public static async fetchNFT(id: string) {
        let data = await this.performGet<ArrayBuffer>(id, { responseType: "arraybuffer" });
        return Buffer.from(data);
    }

    public static isNiftySubgroup(group: string) {
        try {
            let addrBuf = nexcore.Address.decodeNexaAddress(group).getHashBuffer();
            if (!nexcore.GroupToken.isSubgroup(addrBuf)) {
                return false;
            }
    
            return addrBuf.subarray(0, 32).toString('hex') === this.NIFTY_TOKEN.tokenIdHex;
        } catch {
            return false;
        }
    }

    private static async performGet<T>(url: string, config?: AxiosRequestConfig) {
        try {
            const { data } = await axios.get<T>(this.endpoint + url, config);
            return data;
        } catch (e) {
            if (axios.isAxiosError<IErrorResponse>(e) && e.response?.data) {
                throw new Error(e.response.data.message);
            } else {
                console.error(e);
                throw new Error("Unexpected Error: " + (e instanceof Error ? e.message : "see log for details"));
            }
        }
    }
}