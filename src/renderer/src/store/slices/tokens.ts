import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { TokenCardDataDynamic } from "@renderer/components/atoms/token-card";

export interface TokensState {
    tokensData: { [key: string]: TokenCardDataDynamic };
}
  
const initialState: TokensState = {
    tokensData: {},
}

export const tokensSlice = createSlice({
    name: 'tokens',
    initialState,
    reducers: {
        setTokenData: (state, action: PayloadAction<{id: string, data: TokenCardDataDynamic}>) => {
            state.tokensData[action.payload.id] = action.payload.data;
        },
    }
});

export const { setTokenData } = tokensSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const tokensState = (state: RootState) => state.tokens.tokensData;

export default tokensSlice.reducer;