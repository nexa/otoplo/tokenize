import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { Balance, WalletKeys } from "../../core/wallet/interfaces";
import StorageProvider from "../../providers/storageProvider";
import * as WalletUtils from "../../core/wallet/walletUtils";
import HDPrivateKey from "nexcore-lib/types/lib/hdprivatekey";
import { TransactionEntity } from "../../core/db/entities";

export interface WalletState {
    initLoading: boolean,
    accountKey?: HDPrivateKey,
    keys: WalletKeys,
    balance: Balance
}
  
const initialState: WalletState = {
    initLoading: false,
    keys: {receiveKeys: [], changeKeys: []},
    balance: {confirmed: "0", unconfirmed: "0"}
}

export const fetchBalance = createAsyncThunk('wallet/fetchBalance', async (_, thunkAPI) => {
    let rootState = thunkAPI.getState() as RootState;
    let state = rootState.wallet;

    let rAddrs = state.keys.receiveKeys.map(k => k.address);
    let cAddrs = state.keys.changeKeys.map(k => k.address);

    let balances = await WalletUtils.fetchTotalBalance(rAddrs.concat(cAddrs));
    return WalletUtils.sumBlance(balances);
});

export const syncWallet = createAsyncThunk('wallet/syncWallet', async (_, thunkAPI) => {
    let rootState = thunkAPI.getState() as RootState;
    let state = rootState.wallet;

    let fromHeight = StorageProvider.getTransactionsStatus().height;

    let receiveAddresses = state.keys.receiveKeys.map(ak => ak.address);
    let changeAddresses = state.keys.changeKeys.map(ak => ak.address);
    let allAddresses = receiveAddresses.concat(changeAddresses);

    let rTxs = WalletUtils.fetchTransactionHistory(receiveAddresses, fromHeight);
    let cTxs = WalletUtils.fetchTransactionHistory(changeAddresses, fromHeight);

    let [rData, cData] = await Promise.all([rTxs, cTxs]);
    let rIdx = rData.index + 1, cIdx = cData.index + 1;

    let txHistory = rData.txs;
    for (let tx of cData.txs.values()) {
        txHistory.set(tx.tx_hash, tx);
    }

    let updateBalance = false;
    let txPromises: Promise<TransactionEntity>[] = [];
    let correlationId = crypto.randomUUID();
    for (let tx of txHistory.values()) {
        updateBalance = true;
        let t = WalletUtils.classifyAndSaveTransaction(tx, allAddresses, correlationId);
        txPromises.push(t);
    }

    let updateWalletKeys = false;
    let walletKeys = state.keys;
    let balance = { confirmed: "0", unconfirmed: "0" };
    if (updateBalance) {
        let walletIdx = StorageProvider.getWalletIndexes();
        if (walletIdx.receiveIndex < rIdx || walletIdx.changeIndex < cIdx) {
            updateWalletKeys = true;
            walletKeys = WalletUtils.generateKeysAndAddresses(state.accountKey as HDPrivateKey, 0, rIdx, 0, cIdx);
            StorageProvider.saveWalletIndexes({ receiveIndex: rIdx, changeIndex: cIdx });
        }
        let rAddrs = walletKeys.receiveKeys.map(k => k.address);
        let cAddrs = walletKeys.changeKeys.map(k => k.address);
        let balances = await WalletUtils.fetchTotalBalance(rAddrs.concat(cAddrs));
        balance = WalletUtils.sumBlance(balances);
    }

    await Promise.all(txPromises);

    if (fromHeight < Math.max(rData.lastHeight, cData.lastHeight)) {
        StorageProvider.setTransactionsStatus({ height: Math.max(rData.lastHeight, cData.lastHeight) });
    }

    return {updateBalance: updateBalance, updateKeys: updateWalletKeys, balance: balance, walletKeys: walletKeys};
});

export const walletSlice = createSlice({
    name: 'wallet',
    initialState,
    reducers: {
        setAccountKey: (state, action: PayloadAction<HDPrivateKey>) => {
            state.accountKey = action.payload;
        },
        setKeys: (state, action: PayloadAction<WalletKeys>) => {
            state.keys = action.payload;
        },
        startInitLoading: state => {
            state.initLoading = true;
        },
        stopInitLoading: state => {
            state.initLoading = false;
        },
    },
    extraReducers(builder) {
        builder
            .addCase(fetchBalance.fulfilled, (state, action) => {
                state.balance = action.payload;
            })
            .addCase(fetchBalance.rejected, (_state, action) => {
                console.log(action.error.message);
            })
            .addCase(syncWallet.fulfilled, (state, action) => {
                let payload = action.payload
                if (payload.updateBalance) {
                    state.balance = payload.balance;
                }
                if (payload.updateKeys) {
                    state.keys = payload.walletKeys;
                }
                StorageProvider.removeLock(StorageProvider.SYNC_LOCK);
            })
            .addCase(syncWallet.rejected, (_state, action) => {
                console.log(action.error.message);
                StorageProvider.removeLock(StorageProvider.SYNC_LOCK);
            })
    },
});

export const { setAccountKey, setKeys, startInitLoading, stopInitLoading } = walletSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const walletState = (state: RootState) => state.wallet;

export default walletSlice.reducer;