import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";

export interface LoadingState {
    value: boolean
}
  
const initialState: LoadingState = {
    value: false
}

export const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
        startLoading: state => {
            state.value = true;
        },
        stopLoading: state => {
            state.value = false;
        },
    }
});

export const { startLoading, stopLoading } = loadingSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const loadingState = (state: RootState) => state.loading.value;

export default loadingSlice.reducer;