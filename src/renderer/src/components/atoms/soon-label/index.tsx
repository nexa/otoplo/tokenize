import styles from './soon-label.module.css';

export default function SoonLabel() {
    return (
        <h5 className={styles['soon-label']}>This feature is coming soon...</h5>
    );
}
