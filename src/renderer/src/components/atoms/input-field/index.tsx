import { Dispatch, SetStateAction, ClipboardEvent } from 'react';
import CopyPasteButton from '../mini-button';
import styles from './input-field.module.css';

interface InterfaceInputField {
    label: string;
    size: 'long' | 'short' | 'full';
    address?: boolean;
    id?: string;
    name?: string;
    dot?: boolean;
    onChange: any;
    onClick?: any;
    value?: string | number;
    required?: boolean;
    setPaste?: Dispatch<SetStateAction<string>>;
    inputType?: string;
    step?: string;
    hidden?: boolean;
    placeholder?: string;
}

export default function InputField({
    label,
    size = 'long',
    address = false,
    dot = false,
    id,
    name,
    onChange,
    value,
    required,
    setPaste,
    inputType,
    step,
    hidden,
    placeholder
}: InterfaceInputField) {
    const handlePaste = async (event: ClipboardEvent<HTMLInputElement>) => {
        if (setPaste && address) {
            event.preventDefault();
            const pastedText = await navigator.clipboard.readText();
            setPaste(pastedText);
        }
    };

    return (
        <label className={styles['input-field']}>
            {label}
            {required && '*'}
            {dot ? '.' : ':'}
            {address && setPaste && (
                <CopyPasteButton onClick={handlePaste} type={'paste'} />
            )}
            <input
                className={styles[size]}
                placeholder={hidden ? "Hidden" : (placeholder || "Enter...")}
                id={id}
                value={value}
                name={name}
                onChange={onChange}
                required={required}
                type={inputType || 'text'}
                inputMode={inputType === 'number' ? 'numeric' : undefined}
                step={step || '0'}
                disabled={hidden}
            />
        </label>
    );
}
