import Skeleton from 'react-loading-skeleton';
import styles from './kill-switch.module.css';

interface InterfaceKillSwitch {
    disabled?: boolean;
    onClick?: () => void;
    isLoading?: boolean;
}

export default function KillSwitch({
    disabled,
    onClick,
    isLoading = false,
}: InterfaceKillSwitch) {
    return (
        <>
            {isLoading ? (
                <Skeleton width={22} height={22} />
            ) : (
                <button
                    onClick={onClick}
                    className={styles['kill-switch-button']}
                    disabled={disabled}
                />
            )}
        </>
    );
}
