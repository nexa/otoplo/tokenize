import Skeleton from 'react-loading-skeleton';
import styles from './nfts-card.module.css';
import { NftEntity } from '@renderer/core/db/entities';
import { useEffect, useState } from 'react';
import JSZip from 'jszip';
import { isNullOrEmpty } from '@renderer/utils/utils';

export interface InterfaceNftCard {
    nftEntity?: NftEntity;
    nftNumber?: number;
    isLoading?: boolean;
    showNft?: () => void;
}

export default function NftCard({
    nftEntity,
    nftNumber,
    isLoading,
    showNft,
}: InterfaceNftCard) {

    const [nftName, setNftName] = useState('');
    const [image, setImage] = useState('');

    useEffect(() => {
        async function loadData(zipData: Buffer) {
            let zip = await JSZip.loadAsync(zipData);
            let info = zip.file('info.json');
            if (info) {
                let infoJson = await info.async('string');
                let infoObj = JSON.parse(infoJson);
                if (infoObj.title) {
                    setNftName(infoObj.title);
                }
            }
            let pubImg = zip.file(/^public\./);
            let frontImg = zip.file(/^cardf\./);
            if (!isNullOrEmpty(pubImg)) {
                let img = await pubImg[0].async('blob');
                let url = URL.createObjectURL(img);
                setImage(url);
            } else if (!isNullOrEmpty(frontImg)) {
                let img = await frontImg[0].async('blob');
                let url = URL.createObjectURL(img);
                setImage(url);
            }
        }

        if (nftEntity) {
            loadData(nftEntity.zipData).catch(e => {
                console.log(e)
            });
        }
    }, [nftEntity]);

    return (
        <>
            {isLoading ? (
                <Skeleton
                    width={120}
                    height={170}
                    style={{ marginRight: '10px' }}
                />
            ) : (
                <div className={styles['nfts-card']} onClick={showNft}>
                    <img src={image} alt={`${nftName} nft`} />
                    <div className={styles['nft-box']}>
                        <h5>#{nftNumber}</h5>
                        <p>{nftName}</p>
                    </div>
                </div>
            )}
        </>
    );
}
