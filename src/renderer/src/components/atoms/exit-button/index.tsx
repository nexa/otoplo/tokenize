import styles from './exit-button.module.css';

interface InterfaceExitButton {
    onClick: () => void;
}

export default function ExitButton({ onClick }: InterfaceExitButton) {
    return <button onClick={onClick} className={styles['exit-button']} />;
}
