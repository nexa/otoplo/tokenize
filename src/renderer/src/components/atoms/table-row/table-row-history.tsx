import bigDecimal from 'js-big-decimal';
import { TransactionEntity } from '../../../core/db/entities';
import { parseAmountWithDecimals, truncateStringMiddle } from '../../../utils/utils';
import MiniButton from '../mini-button';
import styles from './table-row.module.css';
import nexaLogo from '../../../assets/nexa-logo.svg';
import dummy from '../../../assets/token-icon-placeholder.svg';
import dummyAlert from '../../../assets/token-icon-placeholder-w-alert.svg';
import { useNavigate } from 'react-router-dom';
import { tokensRepository } from '@renderer/core/db/tokens.repository';
import { useEffect, useState } from 'react';
import { parseTokenTxAction } from '@renderer/core/wallet/walletUtils';
import NiftyProvider from '@renderer/providers/niftyProvider';

export default function TableRowHistory({ rowData }: { rowData: TransactionEntity }) {
    const navigate = useNavigate();

    const redirectToImport = (group: string) => {
        navigate('/import-token-group-page', { state: { groupAddr: group } });
    }

    const redirectToGroup = (group: string) => {
        navigate(`/token-group-page/${group}`);
    }

    const [asset, setAsset] = useState();
    const [tokenAmount, setTokenAmount] = useState('');

    let dateAndTime = "Pending";
    if (rowData.height > 0) {
        let date = new Date(rowData.time * 1000).toLocaleDateString();
        let time = new Date(rowData.time * 1000).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit", hour12: false });
        dateAndTime = date + " " + time;
    }

    const nexaAmount = new bigDecimal(rowData.value).divide(new bigDecimal(100), 2);
    const nexaFee = new bigDecimal(rowData.fee).divide(new bigDecimal(100), 2);

    let actionStyle = {};
    let action = '';
    let nexaAmountVal = '';
    if (rowData.state === 'outgoing') {
        actionStyle = {color: `var(--send)`}
        action = 'Send';
        nexaAmountVal = nexaAmount.add(nexaFee).getPrettyValue();
    } else if (rowData.state === 'incoming') {
        actionStyle = {color: `var(--receive)`}
        action = 'Receive';
        nexaAmountVal = nexaAmount.getPrettyValue();
    } else {
        if (rowData.group === 'none') {
            action = 'Consolidate'
        } else {
            let op = parseTokenTxAction(rowData.txGroupType, rowData.state);
            actionStyle = {color: `var(--${op})`}
            action = op.charAt(0).toUpperCase() + op.slice(1);
            if (rowData.extraGroup !== 'none') {
                action = 'Subgroup ' + action;
                actionStyle = {color: `var(--subgroup)`}
            }
        }
        nexaAmountVal = nexaFee.getPrettyValue();
    }

    useEffect(() => {
        let asset: any;
        if (rowData.group === 'none') {
            asset = <img title='NEXA' src={nexaLogo} alt="NEXA" />
            setAsset(asset);
            setTokenAmount('---');
        } else {
            let tAmount = new bigDecimal(rowData.tokenAmount).getPrettyValue();
            let group = rowData.group;
            if (NiftyProvider.isNiftySubgroup(group)) {
                group = NiftyProvider.NIFTY_TOKEN.token;
            }
            tokensRepository.findById(group).then(res => {
                if (res) {
                    asset = <img style={{ cursor: 'pointer' }} onClick={() => redirectToGroup(group)} title={res.ticker ?? res.token} src={res.iconUrl || dummy} alt='TOKEN' />;
                    tAmount = parseAmountWithDecimals(rowData.tokenAmount, res.decimals);
                } else {
                    asset = <img style={{ cursor: 'pointer' }} onClick={() => redirectToImport(group)} title={group} src={dummyAlert} alt="TOKEN" />;
                }
                setAsset(asset);
                setTokenAmount(tAmount);
            })
        }
    }, [rowData]);

    return (
        <tr className={styles['tr']}>
            <td>{dateAndTime}</td>
            <td>{asset}</td>
            <td
                style={{
                    color: `var(--hyperlink)`,
                    display: 'flex',
                    alignItems: 'center',
                    height: '50px',
                    gap: '10px',
                }}
            >
                <a
                    href={`https://explorer.nexa.org/tx/${rowData.idem}`}
                    className={styles['link']}
                    target={'_blank'}
                    rel='noreferrer'
                >
                    {truncateStringMiddle(rowData.idem, 20)}
                </a>
                <MiniButton text={rowData.idem} type={'copy'} />
            </td>
            <td>{nexaAmountVal}</td>
            <td>{tokenAmount}</td>
            <td style={actionStyle}>
                {action}
            </td>
        </tr>
    );
}
