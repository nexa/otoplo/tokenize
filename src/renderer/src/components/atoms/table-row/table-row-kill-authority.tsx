import CheckMark from '../check-mark';
import { AuthorityData } from '../token-card';
import styles from './table-row.module.css';

export default function TableRowKillAuthority({
    rowData
}: {
    rowData: AuthorityData;
}) {
    return (
        <tr>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.authorise} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.mint} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.melt} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.rescript} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.subgroup} />
            </td>
        </tr>
    );
}
