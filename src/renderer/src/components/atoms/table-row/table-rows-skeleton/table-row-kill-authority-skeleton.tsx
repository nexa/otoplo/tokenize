import CheckMark from '../../check-mark';
import styles from '../table-row.module.css';

export default function TableRowKillAuthoritySkeleton() {
    const columns = [
        { className: styles['tr-skeleton-autorise'] },
        { className: styles['tr-skeleton-mint'] },
        { className: styles['tr-skeleton-melt'] },
        { className: styles['tr-skeleton-rescript'] },
        { className: styles['tr-skeleton-subgroup'] },
    ];

    return (
        <tr className={styles['tr']}>
            {columns.map((column, index) => (
                <td key={index} className={column.className}>
                    <CheckMark isLoading={true} />
                </td>
            ))}
        </tr>
    );
}
