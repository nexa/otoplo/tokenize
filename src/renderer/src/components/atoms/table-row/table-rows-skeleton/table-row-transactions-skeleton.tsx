import Skeleton from 'react-loading-skeleton';
import styles from '../table-row.module.css';

export default function TableRowTransactionsSkeleton() {
    const columns = [
        { className: styles['td-transactions-skeleton-type'] },
        { className: styles['td-transactions-skeleton-txid'] },
        { className: styles['td-transactions-skeleton-amount'] },
        { className: styles['td-transactions-skeleton-date'] },
    ];

    return (
        <tr className={styles['tr']}>
            {columns.map((column, index) => (
                <td key={index} className={column.className}>
                    <Skeleton height={30} />
                </td>
            ))}
        </tr>
    );
}
