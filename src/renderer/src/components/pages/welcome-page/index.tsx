import { useState } from 'react';
import SimpleButton from '../../atoms/simple-button';
import TokenizeTitle from '../../atoms/tokenize-title';
import Popup from '../../organism/popup';
import {
    RestoreWalletPopup,
    CreateWalletPopup,
    EnterPasswordPopup,
} from '../../molecules/popups';
import styles from './welcome-page.module.css';
import { getEncryptedSeed } from '../../../core/wallet/seedUtils';

const buttonsList = [
    {
        id: '1',
        label: 'Restore Wallet',
        popupType: 'restore-wallet-popup',
        type: 'border' as 'border',
        color: 'primary' as 'primary',
    },
    {
        id: '2',
        label: 'Open Wallet',
        popupType: 'enter-password-popup',
        type: 'colored' as 'colored',
        color: 'primary' as 'primary',
    },
    {
        id: '3',
        label: 'Create Wallet',
        popupType: 'create-wallet-popup',
        type: 'border' as 'colored',
        color: 'primary' as 'primary',
    },
];

export default function WelcomePage({
    AuthorizedOn,
}: {
    AuthorizedOn: (mnemonic: string) => void;
}) {
    const [popUpStatus, setPopUpStatus] = useState<string | null>();
    const [showPopUp, setShowPopUp] = useState<boolean>(false);

    const handleOpenPopUp = (popUp: string) => {
        setShowPopUp(true);
        setPopUpStatus(popUp);
    };

    const handleClosePopUp = () => {
        setShowPopUp(false);
        setPopUpStatus(null);
    };

    const renderPopup = () => {
        switch (popUpStatus) {
            case 'restore-wallet-popup':
                return <RestoreWalletPopup AuthorizedOn={AuthorizedOn} />;
            case 'create-wallet-popup':
                return <CreateWalletPopup AuthorizedOn={AuthorizedOn} />;
            case 'enter-password-popup':
                return <EnterPasswordPopup AuthorizedOn={AuthorizedOn} />;
            default:
                return null;
        }
    };

    const encSeed = getEncryptedSeed();

    return (
        <div className={styles['welcome-page']}>
            {showPopUp && (
                <Popup isMini={popUpStatus === 'enter-password-popup'} close={handleClosePopUp}>{renderPopup()}</Popup>
            )}
            <TokenizeTitle />
            <h2 className={styles['page-description']}>
                Launch your next token project!
            </h2>
            <div className={styles['button-controls']}>
                {buttonsList.map((el) => {
                    if (el.id === '2' && !encSeed) {
                        return '';
                    }
                    
                    return <SimpleButton
                                key={el.id}
                                label={el.label}
                                onClick={() => handleOpenPopUp(el.popupType)}
                                type={el.type}
                                color={el.color}
                            />
                })}
            </div>
        </div>
    );
}
