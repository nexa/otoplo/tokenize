import { ChangeEvent, ReactElement, useState } from 'react';
import PageTitle from '../../atoms/page-title';
import RequiredLabel from '../../atoms/required-label';
import SimpleButton from '../../atoms/simple-button';
import CreateTokenPopup from '../../molecules/popups/create-token-popup';
import Popup from '../../organism/popup';
import styles from './create-token-groupt-page.module.css';
import { useLocation } from 'react-router-dom';
import InputField from '@renderer/components/atoms/input-field';
import { calcJsonDocHash, currentTimestamp, isNullOrEmpty, isValidUrl, signJsonDoc } from '@renderer/utils/utils';
import { AlertPopup } from '@renderer/components/molecules/popups';
import TokenApiProvider from '@renderer/providers/tokenApiProvider';
import { buildAndSignCreateGroupTransaction, buildAndSignCreateSubgroupTransaction } from '@renderer/core/wallet/txUtils';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import { TokenEntity } from '@renderer/core/db/entities';
import PrivateKey from 'nexcore-lib/types/lib/privatekey';
import { tokensRepository } from '@renderer/core/db/tokens.repository';

export default function CreateTokenGroupPage() {
    const [subData, setSubData] = useState('');
    const [name, setName] = useState('');
    const [ticker, setTicker] = useState('');
    const [decimals, setDecimals] = useState<number>();
    const [metadataUrl, setMetadataUrl] = useState('');
    const [jsonStr, setJsonStr] = useState('');
    const [jsonSig, setJsonSig] = useState('');
    const [loading, setLoading] = useState(false);
    const [tx, setTx] = useState<Transaction>();
    const [tokenEntity, setTokenEntity] = useState<TokenEntity>();

    const wallet = useAppSelector(walletState);

    const [showSuccess, setShowSuccess] = useState(false);
    const [showPopUp, setShowPopUp] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');

    const location = useLocation();
    const parent = location.state ? location.state.parent : '';

    const handleChangeSubData = (e: ChangeEvent<HTMLInputElement>) => {
        setSubData(e.target.value);
    };

    const handleChangeName = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };

    const handleChangeTicker = (e: ChangeEvent<HTMLInputElement>) => {
        setTicker(e.target.value);
    };

    const handleChangeDecimals = (e: ChangeEvent<HTMLInputElement>) => {
        if (!e.target.value) {
            setDecimals(undefined);
        } else {
            setDecimals(parseFloat(e.target.value));
        }
    };

    const handleChangeDocUrl = (e: ChangeEvent<HTMLInputElement>) => {
        setMetadataUrl(e.target.value);
    };

    const handleSubmit = async () => {
        try {
            let jsonString = '', jsonHash = '', iconUrl = '', summary = '';
            setJsonStr('');
            setJsonSig('');
            setLoading(true);

            if (parent && isNullOrEmpty(subData)) {
                throw new Error("Subgroup Data is missing");
            }
            if (isNullOrEmpty(name) || name.length > 20) {
                throw new Error("Name must be up to 20 characters");
            }
            if (isNullOrEmpty(ticker) || ticker.length > 8) {
                throw new Error("Ticker must be up to 8 characters");
            }
            let decimalVal = decimals ?? 0;
            if (!Number.isInteger(decimalVal) || decimalVal < 0 || decimalVal > 18) {
                throw new Error("Decimals must be between 0 and 18");
            }
            if (!isNullOrEmpty(metadataUrl)) {
                if (!isValidUrl(metadataUrl, ['http', 'https'])) {
                    throw new Error("Invalid Json URL");
                }

                let json = await TokenApiProvider.getJsonDoc(metadataUrl);
                jsonString = json.jsonStr;
                setJsonStr(json.jsonStr);
                jsonHash = calcJsonDocHash(json.jsonStr);
                if (Array.isArray(json.json)) {
                    summary = json.json[0]?.summary ?? "";
                    if (summary && summary.trim() && summary.trim().at(-1) !== '.') {
                        summary += ".";  
                    }
                    summary += " " + (json.json[0]?.description ?? "");

                    if (json.json[0]?.icon) {
                        let icon = json.json[0].icon;
                        if (typeof icon === 'string') {
                            if (icon.startsWith('http')) {
                                iconUrl = icon;
                            } else {
                                var url = new URL(metadataUrl);
                                iconUrl = `${url.origin}${icon}`;
                            }
                        }
                    }
                }
            }

            let res: { signKey: PrivateKey; tx: Transaction; tokenId: string; idHex: string; };
            if (parent) {
                res = await buildAndSignCreateSubgroupTransaction(wallet.keys, parent, subData, name, ticker, decimalVal, metadataUrl, jsonHash);

                let isExistInDB = await tokensRepository.findById(res.idHex);
                if (isExistInDB) {
                    throw new Error("Subgroup with the same parent and data already exist");
                }

                let isExistInNetwork = await TokenApiProvider.isTokenExist(res.tokenId);
                if (isExistInNetwork) {
                    throw new Error("Subgroup with the same parent and data already exist");
                }
            } else {
                res = await buildAndSignCreateGroupTransaction(wallet.keys, name, ticker, decimalVal, metadataUrl, jsonHash);
            }

            if (!isNullOrEmpty(metadataUrl)) {
                setJsonSig(signJsonDoc(jsonString, res.signKey!));
            }
            setTx(res.tx);
            setTokenEntity({
                addedTime: currentTimestamp(),
                decimals: decimalVal,
                name: name,
                ticker: ticker,
                parentGroup: parent,
                docUrl: metadataUrl,
                docHash: jsonHash,
                iconUrl: iconUrl,
                token: res.tokenId,
                tokenIdHex: res.idHex,
                summary: summary
            })
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : 'An error occured, try again later.');
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    };

    const confirmTx = (idem: string, token: string, sig?: string) => {
        setShowPopUp(false);
        setAlertMessage(<>
                            <div>TX IDEM:</div>
                            <div style={{wordBreak: 'break-all'}}>{idem}</div><br/>
                            <div>Token:</div>
                            <div style={{wordBreak: 'break-all'}}>{token}</div><br/>
                            { sig ? (
                                <>
                                <div>Json Signature:</div>
                                <div style={{wordBreak: 'break-all'}}>{sig}</div><br/>
                                <div><b>* Save the signature and add it to your json</b></div>
                                </>
                            ) : (
                                ''
                            )}
                        </>);
        setShowSuccess(true);
    }

    return (
        <div className={styles['create-token-groupt-page']}>
            <PageTitle label={parent ? "Create Token Subgroup" : "Create Token Group"} />
            <h6>
                Use this page to generate new group token, or subgroup
                of an existing parent group. Simply enter all the required
                information and click 'create'.
            </h6>
            { parent && 
                <div className={styles['parent-label']}>
                    <span className={styles['label']}>Parent Group:</span>
                    <span className={styles['parent-val']}>{parent}</span>
                </div>
            }
            <form onSubmit={handleSubmit}>
                { parent &&
                    <InputField
                        label='Subgroup Data'
                        size='long'
                        onChange={handleChangeSubData}
                        required
                        value={subData}
                    />
                }
                <InputField
                    label='Name'
                    size='long'
                    onChange={handleChangeName}
                    required
                    value={name}
                />
                <InputField
                    label='Ticker'
                    size='long'
                    onChange={handleChangeTicker}
                    required
                    value={ticker}
                />
                <InputField
                    label='Decimals'
                    size='long'
                    inputType='number'
                    onChange={handleChangeDecimals}
                    required
                    value={decimals}
                />
                <InputField
                    label='Metadata URL'
                    size='long'
                    onChange={handleChangeDocUrl}
                    value={metadataUrl}
                />
            </form>
            <div className={styles['footer']}>
                <RequiredLabel />
                <SimpleButton
                    label={'Create'}
                    type={'colored'}
                    color={'primary'}
                    isSubmit={true}
                    onClick={handleSubmit}
                    isLoading={loading}
                />
            </div>
            { showPopUp && 
                <Popup close={() => setShowPopUp(false)} title={'Create Token Confirmation'}>
                    <CreateTokenPopup tokenEntity={tokenEntity!} jsonStr={jsonStr} jsonSig={jsonSig} tx={tx} confirm={confirmTx}/>
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup close={() => setShowSuccess(false)}>
                    <AlertPopup label='Success' message={alertMessage} close={() => setShowSuccess(false)}/>
                </Popup>
            }
        </div>
    );
}
