import { ChangeEvent, useState, FormEvent, useEffect } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import TokenCard, { TokenCardDataDynamic } from '../../atoms/token-card';
import styles from './import-token-group-page.module.css';
import NoResults from '../../atoms/no-results';
import { getTokenData } from '../../../core/wallet/tokenUtils';
import { useAppDispatch, useAppSelector } from '../../../store/hooks';
import { loadingState, startLoading, stopLoading } from '../../../store/slices/loading';
import Skeleton from 'react-loading-skeleton';
import { useLocation } from 'react-router-dom';
import Popup from '@renderer/components/organism/popup';
import { AlertPopup } from '@renderer/components/molecules/popups';
import { TokenEntity } from '@renderer/core/db/entities';
import { currentTimestamp, parseAmountWithDecimals } from '@renderer/utils/utils';
import { tokensRepository } from '@renderer/core/db/tokens.repository';
import NiftyProvider from '@renderer/providers/niftyProvider';

export default function ImportTokenGroupPage() {
    const location = useLocation();

    const [search, setSearch] = useState(location.state?.groupAddr ?? '');
    const [showNoResults, setShowNoResults] = useState(false);
    const [tokenData, setTokenData] = useState<TokenCardDataDynamic>();
    const [tokenEntity, setTokenEntity] = useState<TokenEntity>();
    const [isNFT, setIsNFT] = useState(false);

    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');
    const [alertLable, setAlertLabel] = useState('');

    const isLoading = useAppSelector(loadingState);
    const dispatch = useAppDispatch();

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
    };

    const handleSearchOp = () => {
        setTokenData(undefined);
        setTokenEntity(undefined);
        setIsNFT(false);
        dispatch(startLoading());

        getTokenData(search).then(res => {
            let tokenData = {} as TokenCardDataDynamic
            
            let supply = parseAmountWithDecimals(res.supply, res.tokenEntry.decimals);
            tokenData.amount = { label: "Supply", amount: supply, rawAmount: res.supply };

            setTokenEntity(res.tokenEntry);
            setTokenData(tokenData);
            setIsNFT(res.tokenEntry.tokenIdHex === NiftyProvider.NIFTY_TOKEN.tokenIdHex);
            setShowNoResults(false);
        }).catch(e => {
            console.log(e)
            setShowNoResults(true);
        }).finally(() => {
            dispatch(stopLoading());
        });
    }
    
    const handleSearch = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        handleSearchOp();
    }

    const handleImport = async () => {
        if (tokenEntity) {
            try {
                dispatch(startLoading());
                if (tokenEntity.parentGroup) {
                    let exist = await tokensRepository.findById(tokenEntity.parentGroup);
                    if (!exist) {
                        let res = await getTokenData(tokenEntity.parentGroup);
                        let pToken = res.tokenEntry;
                        pToken.addedTime = currentTimestamp();
                        await tokensRepository.upsert(pToken);
                    }
                }

                tokenEntity.addedTime = currentTimestamp();
                await tokensRepository.upsert(tokenEntity);
                setAlertLabel('Success');
                setAlertMessage('Imported Successfully!');
                setShowAlert(true);
            } catch (e: any) {
                setAlertLabel('Failed');
                setAlertMessage('Failed to import: ' + e.message);
                setShowAlert(true);
            } finally {
                dispatch(stopLoading());
            }
        } 
    }

    const handleClosePopUp = () => {
        setShowAlert(false);
    };

    useEffect(() => {
        if (search) {
            handleSearchOp();
        }
    }, [])

    return (
        <div className={styles['import-token-group-page']}>
            <PageTitle label="Import Token Group" />
            <h6>
                Use this page to import an existing token group. You will then
                be able to import authority UTXOs to give you the permissions
                you mean to manage that group.
            </h6>
            <form className={styles['search-box']} onSubmit={handleSearch}>
                <InputField
                    label={'Token ID'}
                    size={'full'}
                    value={search}
                    required={true}
                    address={true}
                    onChange={handleChange}
                    setPaste={setSearch}
                />
                <SimpleButton
                    label={'Search'}
                    type={'colored'}
                    isRectangle={true}
                    color={'primary'}
                    isSubmit={true}
                />
            </form>
            <div className={styles['result-box']}>
                { isLoading ? (
                    <Skeleton width={550} height={80}/>
                ) : (
                    tokenEntity ? (
                        <TokenCard
                            tokenInfo={tokenEntity}
                            isNft={isNFT}
                            tokenExtendedInfo={tokenData}
                            handleClickToggle={() => console.log('1')}
                            isImportTokenPage={true}
                        />
                    ) : (
                        showNoResults && <NoResults />
                    )
                )}
                
            </div>
            { tokenData && 
                <div className={styles['import-btn']}>
                    <SimpleButton
                        label={'import'}
                        type={'colored'}
                        color={'primary'}
                        onClick={handleImport}
                    />
                </div>
            }
            { showAlert &&
                <Popup isTiny close={handleClosePopUp}>
                    <AlertPopup label={alertLable} message={alertMessage} close={handleClosePopUp}/>
                </Popup>
            }
        </div>
    );
}
