import { useLiveQuery } from 'dexie-react-hooks';
import PageTitle from '../../atoms/page-title';
import Table from '../../organism/table';
import styles from './history-page.module.css';
import { transactionsRepository } from '@renderer/core/db/transactions.repository';

const tableHistoryHeaders = [
    'date',
    'asset',
    'txidem',
    'nexa',
    'tokens',
    'action',
];

export default function HistoryPage() {
    const data = useLiveQuery(() => transactionsRepository.findAll());

    return (
        <div className={styles['history-page']}>
            <PageTitle label="History" />
            <div className={styles['table-container']}>
                <Table
                    headers={tableHistoryHeaders}
                    data={data}
                    tableType={'history'}
                />
            </div>
        </div>
    );
}
