import { ReactElement, useEffect, useState } from 'react';
import PageTitle from '../../atoms/page-title';
import styles from './settings-page.module.css';
import { rostrumProvider } from '@renderer/providers/rostrumProvider';
import StorageProvider from '@renderer/providers/storageProvider';
import SimpleButton from '@renderer/components/atoms/simple-button';
import Popup from '@renderer/components/organism/popup';
import { AlertPopup, CreateWalletPopup, EditRostrumPopup, EnterPasswordPopup } from '@renderer/components/molecules/popups';

export default function SettingsPage() {

    const [tmpVal, setTmpVal] = useState(0);
    const [status, setStatus] = useState('Offline');
    const [version, setVersion] = useState('');
    const [blockHeight, setBlockHeight] = useState<number | undefined>(0);
    const [rostrumInstance, setRostumInstance] = useState('');

    const [rostrumScheme, setRostrumScheme] = useState('');
    const [rostrumHost, setRostrumHost] = useState('');
    const [rostrumPort, setRostrumPort] = useState('');

    const [showPopUp, setShowPopUp] = useState(false);
    const [showReset, setShowReset] = useState(false);
    const [alertMessage, setAlertMessage] = useState<string | ReactElement>('');

    const [showPassword, setShowPassword] = useState(false);
    const [showSeed, setShowSeed] = useState(false);
    const [seed, setSeed] = useState('');

    useEffect(() => {
        let params = StorageProvider.getRostrumParams();
        if (params.host === 'rostrum.otoplo.com') {
            setRostumInstance("Default (Otoplo's Instance)");
        } else {
            setRostrumScheme(params.scheme);
            setRostrumHost(params.host);
            setRostrumPort(params.port.toString())
            setRostumInstance(`${params.scheme}://${params.host}:${params.port}/`);
        }

        window.api.getVersion().then(res => {
            setVersion(res);
        });

        const myInterval = setInterval(() => {
            setTmpVal((prevVal) => prevVal + 1);
        }, 30 * 1000);
    
        return () => clearInterval(myInterval);
    }, []);

    useEffect(() => {
        rostrumProvider.getBlockTip()
        .then(res => {
            setBlockHeight(res.height);
            setStatus('Online');
        }).catch(e => {
            setBlockHeight(undefined);
            setStatus('Offline');
        })
    }, [tmpVal]);

    const resetRostrum = () => {
        setAlertMessage(<>
                            <p>This will reset your Rostrum settings to Otoplo Rostrum instance.</p><br/>
                            <p>After confirmation you will be redirected to home page to login again.</p>
                        </>)
        setShowReset(true);
    }

    const confirmReset = () => {
        StorageProvider.removeRostrumParams();
        window.location.reload();
    }

    const revealSeed = (val: string) => {
        setShowPassword(false);
        setSeed(val);
        setShowSeed(true);
    }

    return (
        <div className={styles['settings-page']}>
            <PageTitle label="Settings" />
            <div className={styles['content-details']}>
                <div>
                    <span>Version:</span>
                    {`v${version}`}
                </div>
                <div>
                    <span>Status:</span>
                    {status}
                </div>
                <div>
                    <span>Block Height:</span>
                    {blockHeight?.toLocaleString()}
                </div>
                <div>
                    <span>Rostrum Instance:</span>
                    <div>
                        {rostrumInstance}
                        <SimpleButton color='primary' label='Edit' type='text-only' isRectangle onClick={() => setShowPopUp(true)}/>
                        { rostrumInstance.startsWith('Default') || <SimpleButton color='primary' label='Reset' type='text-only' isRectangle onClick={resetRostrum}/> }
                    </div>
                </div>
                <div>
                    <span>Backup Seed:</span>
                    <div className={styles['nopad']}>
                        <SimpleButton color='primary' label='Show' type='text-only' isRectangle onClick={() => setShowPassword(true)}/>
                    </div>
                </div>
            </div>
            { showPopUp &&
                <Popup isMini close={() => setShowPopUp(false)} title={'Rostrum Settings'}>
                    <EditRostrumPopup scheme={rostrumScheme} host={rostrumHost} port={rostrumPort} />
                </Popup>
            }
            { showReset &&
                <Popup isMini close={() => setShowReset(false)}>
                    <AlertPopup label='Confirmation' message={alertMessage} btnLabel='Confirm' close={confirmReset}/>
                </Popup>
            }
            { showPassword &&
                <Popup isMini close={() => setShowPassword(false)}>
                    <EnterPasswordPopup AuthorizedOn={revealSeed} />
                </Popup>
            }
            { showSeed &&
                <Popup close={() => setShowSeed(false)}>
                    <CreateWalletPopup AuthorizedOn={revealSeed} isBackup backupWords={seed} />
                </Popup>
            }
        </div>
    );
}
