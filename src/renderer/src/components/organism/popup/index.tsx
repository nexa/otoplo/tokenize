import { ReactNode } from 'react';
import ExitButton from '../../atoms/exit-button';
import PageTitle from '../../atoms/page-title';
import styles from './popup.module.css';

export default function Popup({
    children,
    close,
    title,
    isMini = false,
    isTiny = false,
}: {
    children: ReactNode;
    close: () => void;
    title?: string;
    isMini?: boolean;
    isTiny?: boolean;
}) {
    const style = isTiny ? 'tiny-popup' : isMini ? 'mini-popup' : 'popup';
    return (
        <>
            <div className={styles['shadow']} onClick={close} />
            <div className={styles[style]}>
                <ExitButton onClick={close} />
                {title && <PageTitle label={title} />}
                {children}
            </div>
        </>
    );
}
