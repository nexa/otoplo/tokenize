import { ReactNode } from 'react';
import Sidebar from '../../organism/sidebar';
import styles from './layout.module.css';

interface InterfaceLayout {
    children: ReactNode;
}

export default function Layout({ children }: InterfaceLayout) {
    return (
        <main className={styles['main']}>
            <Sidebar />
            <section className={styles['section']}>{children}</section>
        </main>
    );
}
