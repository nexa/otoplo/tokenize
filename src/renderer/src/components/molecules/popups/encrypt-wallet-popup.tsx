import { FormEvent, useState } from 'react';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import InputsList from '../inputs-list';
import styles from './popups.module.css';
import { encryptAndStoreSeed, validatePassword } from '../../../core/wallet/seedUtils';
import { clearLocalWallet } from '../../../core/wallet/walletUtils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';

interface InterfacePasswordItem {
    label: string;
    value: string | number;
    required?: boolean;
    id: string;
}

const initState: InterfacePasswordItem[] = [
    {
        label: 'Password',
        value: '',
        id: 'password',
        required: true
    },
    {
        label: 'Confirm Password',
        value: '',
        id: 'confirm password',
        required: true
    },
];

export default function EncryptWalletPopup({
    mnemonic,
    AuthorizedOn
} : {
    mnemonic: string,
    AuthorizedOn: (mnemonic: string) => void
}) {
    const [password, setPassword] = useState(initState);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        let err = validatePassword(password[0].value+"", password[1].value+"");
        if (err) {
            setAlertMessage(err);
            setShowAlert(true);
        } else {
            clearLocalWallet().then(() => {
                encryptAndStoreSeed(mnemonic, password[0].value+"");
                AuthorizedOn(mnemonic);
            });
        }
    };

    return (
        <div className={styles['encrypt-wallet-popup']}>
            <PageTitle label={'Encrypt Wallet'} />
            <div className={styles['encrypt-wallet-popup-content']}>
                <p>
                    <span>
                        Enter a secure password and make sure to write it down
                        in at least one location where you will not lose it.
                    </span>
                    <span>
                        This password is used to encrypt your wallet and will
                        not be stored anywhere. You will need to enter it each
                        time you interact with the app.
                    </span>
                </p>
                <form onSubmit={handleSubmit}>
                    <InputsList
                        inputsValues={password}
                        setInputsValues={setPassword}
                        inputType={'password'}
                    />
                    <SimpleButton
                        label={'Confirm'}
                        type={'colored'}
                        color={'primary'}
                        isSubmit={true}
                    />
                </form>
            </div>
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
        </div>
    );
}
