import { ReactElement } from 'react';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';

export default function AlertPopup({ label, message, btnLabel, close }: { label: string, message: string | ReactElement, btnLabel?: string, close: () => void }) {
    return (
        <div className={styles['alert-popup']}>
            <PageTitle label={label} />
            <div className={styles['alert-popup-content']}>
                <div>
                    {message}
                </div>
                <SimpleButton
                    label={btnLabel || 'Close'}
                    type={'colored'}
                    color={'primary'}
                    isRectangle
                    onClick={close}
                />
            </div>
        </div>
    );
}
