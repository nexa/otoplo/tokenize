import { ChangeEvent, FormEvent, ReactElement, useState } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import PermissionToggle from '../../atoms/permission-toggle';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { Permission } from '@renderer/components/atoms/token-card';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import { buildAndSignRenewTransaction, buildAuthority } from '@renderer/core/wallet/txUtils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import { isNullOrEmpty } from '@renderer/utils/utils';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import SendConfirmationPopup from './send-confirmation-popup';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';

export default function AuthorisePopup({
    token,
    ticker,
    allPerms,
    close
} : {
    token: string;
    ticker: string;
    allPerms: Permission[];
    close: () => void;
}) {
    const [address, setAddress] = useState('');
    const [permissionsList, setPermissionsList] = useState(allPerms);

    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');
    const [loading, setLoading] = useState(false);

    const [showPopUp, setShowPopUp] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [tx, setTx] = useState<Transaction>();

    const wallet = useAppSelector(walletState);

    const handleChangeAddress = (e: ChangeEvent<HTMLInputElement>) => {
        setAddress(e.target.value);
    };

    const getTogglePermission = (state: 'active' | 'note' | 'disabled') => {
        switch (state) {
            case 'note':
                return 'active';
            case 'active':
                return 'note';
            case 'disabled': 
                return 'disabled';
        }
    };

    const handleClickToggle = (index: number) => {
        setPermissionsList((prevPermissionsList) => {
            const updatedList = [...prevPermissionsList];
            updatedList[index] = {
                ...updatedList[index],
                state: getTogglePermission(updatedList[index].state),
            };
            return updatedList;
        });
    };

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            let toAddress = address || (wallet.keys.receiveKeys.at(-1)?.address ?? '');
            let auths = getPermsAuth();
            
            setLoading(true);
            let tx = await buildAndSignRenewTransaction(wallet.keys, toAddress, auths.amountFlags, token, auths.perms)

            setTx(tx);
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    };

    const getPermsAuth = () => {
        let filteredPerms = permissionsList.filter(p => p.state === 'active');
        if (isNullOrEmpty(filteredPerms)) {
            throw new Error("Authority not selected");
        }
        
        let newPerms = filteredPerms.map(p => p.label);
        return { amountFlags: buildAuthority(newPerms).toString(), perms: newPerms };
    }

    const confirmTx = (idem: string) => {
        setShowPopUp(false);
        setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
        setShowSuccess(true);
    }

    const closeSuccess = () => {
        close();
    }

    return (
        <div className={styles['authorise-popup']}>
            <PageTitle label={'Authorize'} />
            <div className={styles['authorise-popup-content']}>
                <p>
                    Select the permissions you would like to give this new
                    Authority UTXO. Keep in mind that you cannot provide it more
                    permission types than you already have access to in your
                    Tokenize app.
                </p>
                <div className={styles['permissions']}>
                    <h6 className={styles['label']}>Permissions:</h6>
                    <div className={styles['permissions-list']}>
                        {permissionsList.map(
                            (
                                el: Permission,
                                index: number,
                            ) => (
            
                                    <PermissionToggle
                                        key={index}
                                        label={el.label}
                                        state={el.state}
                                        cursorPointer={el.state !== 'disabled'}
                                        onClick={() => handleClickToggle(index)}
                                        isClickableWhenDisabled={true}
                                    />

                            ),
                        )}
                    </div>
                </div>
                <form onSubmit={handleSubmit}>
                    <InputField
                        label={'Address'}
                        size={'long'}
                        value={address}
                        address={true}
                        onChange={handleChangeAddress}
                        setPaste={setAddress}
                    />
                    <div className={styles['control-box']}>
                        <SimpleButton
                            label={'Authorize'}
                            type={'colored'}
                            color={'primary'}
                            isSubmit={true}
                            isLoading={loading}
                        />
                    </div>
                </form>
                <p className={styles['hint']}>
                    *If an address is not entered this authority UTXO will go
                    into your Tokenize wallet.
                </p>
            </div>
            { showPopUp &&
                <Popup close={() => setShowPopUp(false)} title={'Authorize Confirmation'}>
                    <SendConfirmationPopup
                        token={token}
                        ticker={ticker}
                        destination={address}
                        amount={"0"}
                        rawAmount={"0"}
                        auths={permissionsList.filter(p => p.state === 'active').map(p => p.label).join(', ')}
                        tx={tx}
                        txType={TxTokenType.RENEW}
                        confirm={confirmTx}
                    />
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isMini close={closeSuccess}>
                    <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
                </Popup>
            }
        </div>
    );
}
