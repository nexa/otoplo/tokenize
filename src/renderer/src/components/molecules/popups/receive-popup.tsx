import PageTitle from '../../atoms/page-title';
import ReceiveSection from '../receive-section';
import styles from './popups.module.css';

export default function ReceivePopup() {
    return (
        <div className={styles['receive-popup']}>
            <PageTitle label={'Receive'} />
            <div className={styles['receive-popup-content']}>
                <p>
                    <span>
                        Use this address to send tokens or an authority UTXO to your
                        Tokenize app from a different wallet.
                    </span>
                    <span>
                        In case of authority UTXO, this will give you access to the permissions on this
                        authority.
                    </span>
                </p>
                <div>
                    <ReceiveSection />
                </div>
            </div>
        </div>
    );
}
