import { ChangeEvent, useState } from 'react';
import { initStatetoMetadata } from '../../../constants';
import { ReactComponent as DEFAULTCOIN } from '../../../assets/token-icon-placeholder.svg';
import SimpleButton from '../../atoms/simple-button';
import TextareaField from '../../atoms/textarea-field';
import styles from './popups.module.css';
import dummy from '../../../assets/token-icon-placeholder.svg';
import { parseAmountWithDecimals } from '@renderer/utils/utils';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import InputField from '@renderer/components/atoms/input-field';
import { isPasswordValid } from '@renderer/core/wallet/seedUtils';
import { broadcastTransaction, saveLocalTransaction } from '@renderer/core/wallet/txUtils';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';
import Skeleton from 'react-loading-skeleton';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import { TokenEntity } from '@renderer/core/db/entities';
import { tokensRepository } from '@renderer/core/db/tokens.repository';

interface InterfaceSendConfirmationPopup {
    tokenEntity: TokenEntity;
    jsonStr?: string;
    jsonSig?: string;
    tx?: Transaction;
    confirm: (idem: string, token: string, sig?: string) => void;
}

export default function CreateTokenPopup({
    tokenEntity,
    jsonStr,
    jsonSig,
    tx,
    confirm
}: InterfaceSendConfirmationPopup) {

    const [password, setPassword] = useState('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    const validateAndBroadcast = async () => {
        if (password && tx) {
            if (isPasswordValid(password)){
                try {
                    setLoading(true);
                    let idem = await broadcastTransaction(tx.serialize());
                    let dest = 'Payment to yourself';
                    await saveLocalTransaction(tx, idem, dest, "0", 'both', tokenEntity.parentGroup ? TxTokenType.RENEW : TxTokenType.CREATE, tokenEntity.token, tokenEntity.parentGroup);
                    await tokensRepository.upsert(tokenEntity);
                    confirm(idem, tokenEntity.token, jsonSig);
                } catch (e) {
                    setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
                    setShowAlert(true);
                } finally {
                    setLoading(false);
                }
            } else {
                setAlertMessage("Wrond password.");
                setShowAlert(true);
            }
        }
    };

    return (
        <div className={styles['create-token-popup']}>
            {tokenEntity.parentGroup && (
                <p style={{marginBottom: '10px'}}>
                    <span>Parent Group:</span>
                    <span className={styles['bk']}>{tokenEntity.parentGroup}</span>
                </p>
            )}
            <p>
                <span>Name:</span>
                {tokenEntity.name}
            </p>
            <p>
                <span>Ticker:</span>
                {tokenEntity.ticker}
            </p>
            <p>
                <span>Decimals:</span>
                {tokenEntity.decimals}
            </p>
            <TextareaField
                label={'Metadata'}
                placeholder={initStatetoMetadata}
                value={jsonStr}
            />
            <p>
                <span>Fee:</span>
                {parseAmountWithDecimals(tx?.getFee() ?? -1, 2)} NEXA
            </p>
            <div style={{ paddingTop: '30px' }}>
                <InputField
                    label={'Password'}
                    size={'short'}
                    onChange={handlePasswordChange}
                    inputType={'password'}
                />
            </div>
            { tokenEntity.iconUrl ? (
                <img className={styles['icon']} width={200} height={200} src={tokenEntity.iconUrl} onError={e => {
                    e.currentTarget.src = dummy;
                }}/>
            ) : (
                <DEFAULTCOIN/>
            )}
            { loading ? (
                <span className={styles['skeleton']}>
                    <Skeleton width={250} height={50} borderRadius={25}/>
                </span>
            ) : (
                <SimpleButton
                    label={'Confirm'}
                    type={'colored'}
                    color={'primary'}
                    onClick={validateAndBroadcast}
                />
            ) }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
        </div>
    );
}
