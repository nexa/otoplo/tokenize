import PageTitle from '../../atoms/page-title';
import SendSection from '../send-section';
import styles from './popups.module.css';

export default function SendPopup({ token, ticker, decimals, close }: { token: string, ticker: string, decimals: number, close: () => void }) {
    return (
        <div className={styles['send-popup']}>
            <PageTitle label={'Send'} />
            <div className={styles['send-popup-content']}>
                <SendSection token={token} ticker={ticker} decimals={decimals} close={close}/>
            </div>
        </div>
    );
}
