import { ChangeEvent, FormEvent, ReactElement, useState } from 'react';
import InputField from '../../atoms/input-field';
import TableTitle from '../../atoms/table-title';
import SimpleButton from '../../atoms/simple-button';
import SendConfirmationPopup from '../../molecules/popups/send-confirmation-popup';
import Popup from '../../organism/popup';
import styles from './send-section.module.css';
import { MAX_INT64, getRawAmount, isNullOrEmpty, parseAmountWithDecimals } from '@renderer/utils/utils';
import { AlertPopup } from '../popups';
import { buildAndSignTransferTransaction } from '@renderer/core/wallet/txUtils';
import { walletState } from '@renderer/store/slices/wallet';
import { useAppSelector } from '@renderer/store/hooks';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import bigDecimal from 'js-big-decimal';
import { tokensState } from '@renderer/store/slices/tokens';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';

export default function SendSection({ token, ticker, decimals, close }: { token?: string, ticker: string, decimals: number, close?: () => void}) {
  const [toAddress, setToAddress] = useState('');
  const [amount, setAmount] = useState('');
  const [loading, setLoading] = useState(false);

  const [showPopUp, setShowPopUp] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');

  const [tx, setTx] = useState<Transaction>();

  const amountRegx1 = decimals ? new RegExp(`^0\.[0-9]{1,${decimals}}$`) : /^[1-9][0-9]*$/;
  const amountRegx2 = decimals ? new RegExp(`^[1-9][0-9]*(\.[0-9]{1,${decimals}})?$`) : /^[1-9][0-9]*$/;

  const wallet = useAppSelector(walletState);
  const tokens = useAppSelector(tokensState);

  const handleClosePopUp = () => {
    setShowPopUp(false);
  };

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      if (!amountRegx1.test(amount) && !amountRegx2.test(amount)) {
        throw new Error("Invalid Amount.");
      }

      let rawAmount = getRawAmount(amount, decimals);
      if (BigInt(rawAmount) > MAX_INT64) {
        throw new Error(`Cannot send more than ${parseAmountWithDecimals(MAX_INT64, decimals)} in single transaction`);
      }

      let amt = new bigDecimal(rawAmount);
      let balance = new bigDecimal(wallet.balance.confirmed).add(new bigDecimal(wallet.balance.unconfirmed));
      if (token) {
        balance = new bigDecimal(tokens[token].balance);
      }

      if (amt.compareTo(balance) > 0) {
        throw new Error("Insufficient balance.");
      }

      setLoading(true);
      let tx = await buildAndSignTransferTransaction(wallet.keys, toAddress, amt.getValue(), token);

      setTx(tx);
      setShowPopUp(true);
    } catch (e) {
      setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
      setShowAlert(true);
    } finally {
      setLoading(false);
    }
  };

  const confirmTx = (idem: string) => {
    setShowPopUp(false);
    setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
    setShowSuccess(true);
  }

  const handleChangeAddress = (e: ChangeEvent<HTMLInputElement>) => {
    setToAddress(e.target.value);
  };

  const handleChangeAmount = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value === '0' || amountRegx1.test(e.target.value) || amountRegx2.test(e.target.value)) {
      setAmount(e.target.value)
    } else if (isNullOrEmpty(e.target.value)) {
      setAmount('');
    }
  };

  const closeSuccess = () => {
    if (close) {
      close();
    } else {
      setShowSuccess(false);
    }
  }

  return (
    <div className={styles['send-section']}>
        { token ? '' : <TableTitle label="send" /> }
        <form onSubmit={handleSubmit} className={token ? styles['minimal'] : styles['full']}>
            <InputField
                label={'Address'}
                size={'long'}
                value={toAddress}
                address={true}
                onChange={handleChangeAddress}
                setPaste={setToAddress}
            />
            <InputField
                label={'Amount'}
                size={'long'}
                inputType={'number'}
                step="0.01"
                value={amount}
                onChange={handleChangeAmount}
            />
            <SimpleButton
                label={'Send'}
                type={'colored'}
                color={'primary'}
                isSubmit={true}
                isLoading={loading}
            />
        </form>
        { showPopUp &&
            <Popup close={handleClosePopUp} title={'Sending Confirmation'}>
              <SendConfirmationPopup
                token={token}
                ticker={ticker}
                destination={toAddress}
                amount={new bigDecimal(amount).getPrettyValue()}
                rawAmount={getRawAmount(amount, decimals)}
                tx={tx}
                txType={TxTokenType.TRANSFER}
                confirm={confirmTx}
              />
            </Popup>
        }
        { showAlert &&
            <Popup isTiny close={() => setShowAlert(false)}>
                <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
            </Popup>
        }
        { showSuccess &&
            <Popup isMini close={closeSuccess}>
                <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
            </Popup>
        }
    </div>
  )
}
