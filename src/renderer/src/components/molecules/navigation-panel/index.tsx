import { NavLink, useLocation } from 'react-router-dom';
import NavigationPanelItem from '../../atoms/navigation-panel-item';
import SidebarTitle from '../../atoms/sidebar-title';
import styles from './navigation-panel.module.css';

const navigationPanelList = [
    {
        id: '1',
        label: 'Wallet',
        href: 'manage-wallet-page',
    },
    {
        id: '2',
        label: 'Create Token Group',
        href: 'create-token-group-page',
    },
    {
        id: '3',
        label: 'Import Token Group',
        href: 'import-token-group-page',
    },
    {
        id: '4',
        label: 'History',
        href: 'history-page',
    },
    {
        id: '5',
        label: 'Settings',
        href: 'settings-page',
    },
];

export default function NavigationPanel() {
    const location = useLocation();
    return (
        <div className={styles['navigation-panel']}>
            <SidebarTitle label="Actions" />
            <nav className={styles['nav']}>
                {navigationPanelList.map((el) => (
                    <NavLink key={el.id} to={`/${el.href}`}>
                        <NavigationPanelItem
                            label={el.label}
                            isActive={location.pathname === '/' + el.href}
                        />
                    </NavLink>
                ))}
            </nav>
        </div>
    );
}
