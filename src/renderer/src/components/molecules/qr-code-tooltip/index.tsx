import cn from 'classnames';
import QrCode from '../../atoms/qr-code';
import styles from './qr-code-tooltip.module.css';
import { useCloseByClickOutside } from '../../../hooks/useCloseByClickOutSide';
import nexaLogo from '../../../assets/nexa-logo.svg';

export default function QrCodeTooltip({
    url,
    isTokenCard,
    onClose,
}: {
    url: string;
    isTokenCard?: boolean;
    onClose: () => void;
}) {
    const tooltipsClass = cn({
        [styles['qr-code-tooltip']]: true,
        [styles['qr-code-tooltip-token-card']]: isTokenCard,
    });

    const tooltipsTail = cn({
        [styles['qr-code-tooltip-tail']]: true,
        [styles['qr-code-tooltip-tail-token-card']]: isTokenCard,
    });

    const QrCodeTooltipWithClose = useCloseByClickOutside(QrCodeTooltipWrapper);

    function QrCodeTooltipWrapper() {
        return (
            <div className={tooltipsClass}>
                <div className={styles['qr-code-tooltip-content']}>
                    <QrCode mainAddr={url} logo={nexaLogo}/>
                    <div className={tooltipsTail} />
                </div>
            </div>
        );
    }

    return <QrCodeTooltipWithClose onClose={onClose} />;
}
