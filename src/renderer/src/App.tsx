import { useState, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import HistoryPage from './components/pages/history-page';
import ImportTokenGroupPage from './components/pages/import-token-group-page';
import CreateTokenGroupPage from './components/pages/create-token-group-page';
import ManageWalletPage from './components/pages/manage-wallet-page';
import SettingsPage from './components/pages/settings-page';
import TokenGroupPage from './components/pages/token-group-page';
import WelcomePage from './components/pages/welcome-page';
import Layout from './components/templates/layout';
import { discoverWallet, generateAccountKey, generateKeysAndAddresses } from './core/wallet/walletUtils';
import { useAppDispatch } from './store/hooks';
import { fetchBalance, setAccountKey, setKeys, startInitLoading, stopInitLoading, syncWallet } from './store/slices/wallet';
import StorageProvider from './providers/storageProvider';
import { rostrumProvider } from './providers/rostrumProvider';

function App() {
    const [seedphrase, setSeedphrase] = useState('');
    const [tmpVal, setTmpVal] = useState(0);

    const dispatch = useAppDispatch();

    const handleAuthorizedOn = (seedphrase: string) => {
        StorageProvider.removeLock(StorageProvider.SYNC_LOCK);
        setSeedphrase(seedphrase);
    };

    const initWallet = async () => {
        var accountKey = generateAccountKey(seedphrase);
        dispatch(setAccountKey(accountKey));

        var indexes = StorageProvider.getWalletIndexes();
        if (indexes.receiveIndex === 0) {
            indexes = await discoverWallet(accountKey);
        }
        var walletKeys = generateKeysAndAddresses(accountKey, 0, indexes.receiveIndex, 0, indexes.changeIndex);
        
        dispatch(setKeys(walletKeys));
        dispatch(fetchBalance());
    }

    const refreshWallet = () => {
        if (!StorageProvider.setLock(StorageProvider.SYNC_LOCK)) {
            return; // already running
        }
        dispatch(syncWallet());
    }

    useEffect(() => {
        if (seedphrase) {
            dispatch(startInitLoading());
            rostrumProvider.connect().then(() => {
                initWallet().then(() => {
                    dispatch(stopInitLoading());
                    var lastSynced = StorageProvider.getLastSync();
                    if (lastSynced == null || lastSynced < (Math.floor(Date.now() / 1000) - 90)) {
                        refreshWallet();
                    }
                })
            });

            const myInterval = setInterval(() => {
                setTmpVal((prevVal) => prevVal + 1);
            }, 30 * 1000);
        
            return () => clearInterval(myInterval);
        }
        return;
    }, [seedphrase]);

    useEffect(() => {
        if (tmpVal !== 0) {
            refreshWallet();
        }
    }, [tmpVal]);

    return (
        <>
            {seedphrase ? (
                <Layout>
                    <Routes>
                        <Route 
                            path="/history-page" 
                            element={<HistoryPage />} 
                        />
                        <Route
                            path="/import-token-group-page"
                            element={<ImportTokenGroupPage />}
                        />
                        <Route
                            path="/create-token-group-page"
                            element={<CreateTokenGroupPage />}
                        />
                        <Route
                            path="/manage-wallet-page"
                            element={<ManageWalletPage />}
                        />
                        <Route
                            path="/settings-page"
                            element={<SettingsPage />}
                        />
                        <Route
                            path="/token-group-page/:id"
                            element={<TokenGroupPage />}
                        />
                        <Route
                            path="/"
                            element={<ManageWalletPage />}
                        />
                    </Routes>
                </Layout>
            ) : (
                <WelcomePage AuthorizedOn={handleAuthorizedOn} />
            )}
        </>
    );
}

export default App;
